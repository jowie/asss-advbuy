/*
Copyright (c) 2009 Joris v/d Wel

This file is part of ASSS Advanced Buy

   ASSS Advanced Buy is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, version 3 of the License.

   ASSS Advanced Buy is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with ASSS Advanced Buy.  If not, see <http://www.gnu.org/licenses/>.

*/
/**

Advanced Buy

Todo:
	+ crash when you give Upgrade0=abc a RequireUpgrade0 value of test upgrade. Test upgrade is Upgrade1, has an error, gets removed
	+ acronyms for ?buy

Settings:

[Buy]
; Every category that may be used
Category0 = CatA
Category1 = CatB
Category2 = CatC

; Every upgrade that may be used
Upgrade0 = UpA
Upgrade1 = UpB
Upgrade2 = UpC

; Every item that may be used
Item0 = ItemA
Item1 = ItemB
Item2 = ItemC

[Buy_Category_CatA]
Name = Category A
Description = The first category
StaffOnly = 0
Freqs = 1,4,    3

Requirement0 = 2
RequirementValue0 = gdiwafact

Requirement1 = 1
RequirementValue1 = 2


[Buy_Upgrade_UpA]
Name = Upgrade A
Description = The first upgrade
Target = $TARGET_FREQ
Cost = 5000
StaffOnly = $false
Category = CatA

Attribute0 = blah::blah = +4

Freqs = 1,4,5,3

Requirement0 = 2
RequirementValue0 = nodwafact

Action0 = 1
ActionValue0 = 1, 1

[Buy_Item_ItemA]
Name = Upgrade A
Description = The first upgrade
Cost = 5000
StaffOnly = $false
Category = CatA

Freqs = 1,4,5,3

Requirement0 = 2
RequirementValue0 = nodwafact

Action0 = 1
ActionValue0 = 1, 1

*/

#include "advbuy-config.inc"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "asss.h"
#include "gamecredits.h"
#include "attrman.h"
#include "advbuy.h"

#ifdef USE_STRUCTURES
	#define USE_STRUCTURES_BOOL 1
	#include "structures.h"
#else
	#define USE_STRUCTURES_BOOL 0
#endif

#ifdef USE_STATICTURRET
	#define USE_STATICTURRET_BOOL 1
	#include "staticturret.h"
#else
	#define USE_STATICTURRET_BOOL 0
#endif

#ifdef USE_SUPERWEAPON
	#define USE_SUPERWEAPON_BOOL 1
	#include "superweapon.h"
#else
	#define USE_SUPERWEAPON_BOOL 0
#endif

#define CAP_BUYSTAFFITEMS "buystaffitems"

#define MAXIMUMCONFIGENTRIES (1000)
//#define MSG_FUSCHIA (79)

#ifdef NDEBUG
#define assertlm(x) ((void)0)
#else
#define assertlm(e) if (!(e)) AssertLM(#e, __FILE__, __FUNCTION__, __LINE__)
#endif
#define MIN(variable, min) if (variable < (min)) variable = (min)
#define MAX(variable, max) if (variable > (max)) variable = (max)


static Imodman     *mm;
static Ichat       *chat;
static Icapman     *capman;
static Iconfig     *cfg;
static Icmdman     *cmd;
static Iplayerdata *pd;
static Iarenaman   *aman;
static Iattrman    *attrman;
static Icapman     *capman;
static Ilogman     *lm;
static Igame       *game;
static Imapdata    *map;

static int arenaKey = -1;
static int playerKey = -1;

#define BUF_CHARS (511)


#ifndef USE_STRUCTURES
#define STRUCTURES_STRUCTURETYPES 0
#endif

#ifndef USE_SUPERWEAPON
#define SUPERWEAPONS 0
#endif

#define tFREE(a) \
	do \
	{ \
		if (a) \
		{ \
			afree(a); \
			a = NULL; \
		} \
	} while (0)


// Config enum parsing, based up on the one in util.h
#define ENUM(NAME, MAP) \
	enum NAME { MAP(ENUM_HELPER_A, ENUM_HELPER_B) };

#define ENUM_HELPER_A(X, unused, Y) X = Y,
#define ENUM_HELPER_B(X, unused) X,


#define DEFINE_CFGVALPARSER(NAME, MAP) \
	static int NAME(const char *s, int def) \
	{ \
		if (s == NULL) return def; \
		else if ((*s >= '0' && *s <= '9') || *s == '+' || *s == '-') return atoi(s); \
		else if (*s == '$') s++; \
		MAP(DEFINE_CFGVALPARSER_HELPER_A, DEFINE_CFGVALPARSER_HELPER_B) \
		return def; \
	}

#define DEFINE_CFGVALPARSER_HELPER_A(X, CFG, unused) if (strcasecmp(#CFG, s) == 0) return X; else
#define DEFINE_CFGVALPARSER_HELPER_B(X, CFG) if (strcasecmp(#CFG, s) == 0) return X; else


#define CFG_BOOL(A, B) \
	A(BOOL_NO, no, false) \
	A(BOOL_YES, yes, true) \
	A(BOOL_FALSE, false, false) \
	A(BOOL_TRUE, true, true)
ENUM(CFG_BOOL, CFG_BOOL)
DEFINE_CFGVALPARSER(cfgval_bool, CFG_BOOL)

// char*
static HashTable *usedAttributes;	  /// every used attribute for upgrades, this is used to keep the actual strings for attributes (since attributes are mostly stored as linkedlists). The value is just NULL

struct arenaData
{
	Igamecredits *credits;
	bool initialized; /// Has this arena been initialized? (config read, etc)
	bool running;

	long minimumPartialPurchaseForAnnounce;
	HashTable *playerData; /// Holds struct playerData*

	// struct categoryData*
	LinkedList categoriesList;
	HashTable *categories;	  /// every category, both key and value is unique. key = category->config.key

	// struct upgradeData*
	LinkedList upgradesList;
	HashTable *upgrades;	    /// every upgrade, both key and value is unique. key = upgrade->config.key

	// struct itemData*
	LinkedList itemsList;
	HashTable *items;	       /// every freq, both key and value is unique. key = item->config.key

	// struct buyItemData*
	HashTable *buyList;	     /// every category + upgrade + freq, this is used for ?buy. key = buyItem->u.X->config.name

	// struct freqData*
	LinkedList freqs;	       /// every freq that has a category / upgrade / item.

	// struct upgradeData*
	LinkedList purchasedUpgrades;   /// Upgrades with UPGRADETARGET_ARENA that have been purchased

	LinkedList coolList; /// struct coolData. Items that are currently cooling down
};


struct freqData
{
	Arena *arena;
	int freq;

	// struct buyItemData* - there should be no BUYITEM_CATEGORY in this list
	LinkedList buyList;

	// struct upgradeData*
	LinkedList purchasedUpgrades;   /// Upgrades with UPGRADETARGET_FREQ that have been purchased
	LinkedList coolList;
};

struct playerData // stored in a  hashtable in the arenaData
{
	Player *p; // NULL if the player is not in this arena
	Arena *arena;

	// struct upgradeData*
	LinkedList purchasedUpgrades;   /// Upgrades with UPGRADETARGET_PLAYER that have been purchased
	LinkedList coolList;
};

struct playerData_pd // passed to the playerdata module
{
	struct playerData *pdata; // this pointer is changed when the player moves between arenas
};

struct attributeData
{
	const char *property;
	bool addition;
	long value;
	int ship;
};

#define REQUIREMENTTYPE_COUNT (8) //excluding UNSET
#define REQUIREMENTTYPE_MAP(A, B) \
	A(REQUIREMENT_UNSET, unset, -1) \
	A(REQUIREMENT_NONE, none, 0) \
	B(REQUIREMENT_UPGRADE, upgrade) \
	B(REQUIREMENT_STRUCTURETYPE, structuretype) \
	B(REQUIREMENT_STRUCTURE, structure) \
	B(REQUIREMENT_SHIP, ship) \
	B(REQUIREMENT_SAFEZONE, safezone) \
	B(REQUIREMENT_REGION, region) \
	B(REQUIREMENT_SUPERWEAPONCHARGE, superweaponcharge)

ENUM(REQUIREMENTTYPE, REQUIREMENTTYPE_MAP)
DEFINE_CFGVALPARSER(cfgval_requirementType, REQUIREMENTTYPE_MAP)

static bool failedRequirementHides(enum REQUIREMENTTYPE requirementType)
{
	switch(requirementType)
	{
		case REQUIREMENT_UPGRADE:
		case REQUIREMENT_STRUCTURETYPE:
		case REQUIREMENT_STRUCTURE:
		case REQUIREMENT_SHIP:
			return true;
		case REQUIREMENT_UNSET:
		default:
			return false;
	}
}


struct requirementData
{
	enum REQUIREMENTTYPE requirementType;

	union
	{
		struct
		{
			struct upgradeData *upgrade;
		} upgrade;
		
		struct
		{
			int type;
		} structuretype;
		struct
		{
			char *structure;
		} structure;
		
		struct
		{
			u32 ship; // example: to check for terrier you do (requirement.value.ship.ship & 1 << 4)
		} ship;
		//safezone
		struct
		{
			Region *region;
		} region;
		
		struct
		{
#ifdef USE_SUPERWEAPON
			Superweapon superweapon;
#else
			int superweapon;
#endif
			int freqCharge; // in ticks
			int playerCharge;
		} superweaponcharge; //both player and freq
		
	} value;
};

static void freeRequirement(struct requirementData *requirement)
{
	switch(requirement->requirementType)
	{
		case REQUIREMENT_STRUCTURE:
			tFREE(requirement->value.structure.structure);
			break;
		default:
			break;
	}
	afree(requirement);
}



#define ITEMACTION_COUNT (5) //excluding UNSET, including NONE; All the actions MUSt be consecutive
#define ITEMACTION_MAP(A, B) \
	A(ITEMACTION_UNSET, unset, -1) \
	A(ITEMACTION_NONE, none, 0) \
	B(ITEMACTION_PRIZE, prize) \
	B(ITEMACTION_STATICTURRET, staticturret) \
	B(ITEMACTION_SPAWNSTRUCTURE, spawnstructure) \
	B(ITEMACTION_NUKE, nuke)

ENUM(ITEMACTION, ITEMACTION_MAP)
DEFINE_CFGVALPARSER(cfgval_itemAction, ITEMACTION_MAP)


struct actionData
{
	enum ITEMACTION action;

	/*
	This is read as a comma seperated value
	Example:

	Action0 = 1
	ActionValue0 = 5, 2
	*/
	union
	{
		struct
		{
			int type;
			int count;
		} prize;
		
		struct
		{
			const char *key;
		} staticturret;
		struct
		{
			const char *key;
		} spawnstructure;
		struct
		{
			int refundCredits;
			int freqChargeRequired;
		} nuke;
		
	} value;
};

static void freeAction(struct actionData *action)
{
	switch(action->action)
	{
		case ITEMACTION_STATICTURRET:
			tFREE(action->value.staticturret.key);
			break;
		case ITEMACTION_SPAWNSTRUCTURE:
			tFREE(action->value.spawnstructure.key);
			break;
		
		default:
			break;
	}
	afree(action);
}

struct categoryData
{
	struct categoryData_config
	{
		const char *key;	       /// key inside the hash table and ini config
		const char *name;	      /// full name of this upgrade
		const char *description;       /// long description of this upgrade (may be NULL)
		bool staffOnly;	  /// is this category only visible / listable if you have CAP_BUYSTAFFITEMS

		struct categoryData* parentCategory;       /// the parent category, NULL means at the root level

		short *freqs;
		short freq_length;

		//struct requirementData*
		LinkedList requirements; /// Requirements that has to be met before this category is visible in the buy list
	} config;
};

#define UPGRADETARGET_MAP(A, B) \
	B(UPGRADETARGET_PLAYER, player) \
	B(UPGRADETARGET_FREQ, freq) \
	B(UPGRADETARGET_ARENA, arena)
ENUM(UPGRADETARGET, UPGRADETARGET_MAP)
DEFINE_CFGVALPARSER(cfgval_upgradeTarget, UPGRADETARGET_MAP)

struct partialFreqPurchase
{
	struct freqData *fdata;
	long credits;
};

struct upgradeData
{
	AttrmanSetter setter;			/// The attrman setter this upgrade uses

	union
	{
		long arena_credits; // config.target == UPGRADETARGET_ARENA
		LinkedList freqs; // config.target == UPGRADETARGET_FREQ (struct partialFreqPurchase)
	} partialPurchase;

	struct upgradeData_config
	{
		const char *key;			   /// key inside the hash table and ini config
		const char *name;			  /// full name of this upgrade
		const char *description;		   /// long description of this upgrade (may be NULL)

		enum UPGRADETARGET target;	   /// on what has this upgrade effect
		long cost;			    /// how much credits does this upgrade cost

		bool staffOnly;		      /// can this upgrade only be purchased if you have CAP_BUYSTAFFITEMS
		struct categoryData* category;       /// the parent category, NULL means this upgrade is hidden

		short *freqs;
		short freq_length;

		//struct requirementData*
		LinkedList requirements;	     /// Requirements that has to be met before this upgrade is available

		//struct attributeData*
		LinkedList attributes;	       /// attributes this upgrade will set

		//struct actionData*
		LinkedList actions;		  /// The actions this upgrade will perform when enabled


	} config;
};

struct itemData
{
	struct itemData_config
	{
		const char *key;		     /// key inside the hash table and ini config
		const char *name;		    /// full name of this item
		const char *description;	     /// long description of this item (may be NULL)

		long cost;		      /// how much credits does this item cost
		bool staffOnly;		/// can this item only be purchased by staff?
		struct categoryData* category; /// the parent category, NULL means this item is not in any category

		short *freqs;
		short freq_length;

		int arenaCool;
		int freqCool;
		int playerCool;

		//struct actionData*
		LinkedList actions;	    /// The actions this item will perform

		//struct requirementData*
		LinkedList requirements;       /// Requirements that has to be met before this item is available

	} config;
};

struct coolData
{
	ticks_t lastUse;
	struct itemData *item;
};

struct buyItemData
{
	enum
	{
		BUYITEM_ERROR = 0, //dont use
		BUYITEM_CATEGORY,
		BUYITEM_UPGRADE,
		BUYITEM_ITEM
	} type;

	union
	{
		struct categoryData *category;
		struct upgradeData  *upgrade;
		struct itemData     *item;
	} u;
};

static void AssertLM (const char* e, const char* file, const char *function, int line)
{
	if (lm) lm->Log(L_ERROR | L_SYNC, "<advbuy> Assertion \"%s\" failed: file \"%s\", line %d, function %s()\n", e, file, line, function);
	fullsleep(500);
	Error(EXIT_GENERAL, "\nAssertion \"%s\" failed: file \"%s\", line %d, function %s()\n", e, file, line, function);
}

static struct arenaData*		GetArenaData			    (Arena *arena);
static struct freqData *		GetFreqData			     (Arena *arena, int freq, bool allocFreqData);
static struct playerData*	       GetPlayerData			   (Player *p);
static void				 ConstructPlayerData			(struct playerData *pdata);
static void				 DestructPlayerData			(struct playerData *pdata);
static int			      DestructArenaData_HashEnum_categories   (const char *key, void *val, void *clos);
static int			      DestructArenaData_HashEnum_upgrades     (const char *key, void *val, void *clos);
static int			      DestructArenaData_HashEnum_items	(const char *key, void *val, void *clos);
static void			     RemoveUpgradeEverywhere		 (Arena *arena, struct upgradeData *upgrade);
static void			     DestructFreqData			(struct freqData *fdata);
static void			     InitializeArena			 (Arena *arena);
static void			     CleanupArena			    (Arena *arena);
static void			     freeItemData			    (struct itemData *item);
static void			     freeUpgradeData			 (struct upgradeData *upgrade);
static void			     freeCategoryData			(struct categoryData *category);
static int			      DestructArenaData_HashEnum_upgrades     (const char *key, void *val, void *clos);
static int			      DestructArenaData_HashEnum_items	(const char *key, void *val, void *clos);
static void			     ReadConfig			      (Arena *arena);
static bool			     ReadConfig_category		     (Arena *arena, struct categoryData *category);
static bool			     ReadConfig_upgrade		      (Arena *arena, struct upgradeData *upgrade);
static long				 GetRemainingCost			(Player *p, struct upgradeData *upgrade);
static bool			     ReadConfig_item			 (Arena *arena, struct itemData *item);
static bool			     HasUpgrade			      (Player *p, struct upgradeData *upgrade);
static void			     ArenaAction			     (Arena *arena, int action);
static bool			     StartGame			       (Arena *arena);
static void			     StopGame				(Arena *arena);
static void			     EnableUpgrade			   (Target *scope, struct upgradeData *upgrade, bool doActions);
static void			     Cbuy				    (const char *tc, const char *params, Player *p, const Target *target);
static bool			     RequirementsMetCategory		 (Player *p, struct categoryData *category, enum REQUIREMENTTYPE *failedRequirement, bool showMessage);
static bool			     RequirementsMetUpgrade		  (Player *p, struct upgradeData *upgrade, enum REQUIREMENTTYPE *failedRequirement, bool showMessage);
static bool			     RequirementsMetItem		     (Player *p, struct itemData *item, enum REQUIREMENTTYPE *failedRequirement, bool showMessage);
static void			     RemoveUpgrade			   (Target *target, struct upgradeData *upgrade);
static void				 SetUpgradeAttributes			(Target *scope, struct upgradeData *upgrade);
static void				 UnsetUpgradeAttributes			(Target *scope, struct upgradeData *upgrade);

static Iadvbuy advbuy =
{
	INTERFACE_HEAD_INIT(I_ADVBUY, "advbuy")

	StartGame,
	StopGame
};

static int CleanupPlayerData_HashEnum(const char *key, void *val, void *clos)
{
	DestructPlayerData((struct playerData *) val);
	return 1;
}

static void ConstructArenaData(struct arenaData *adata)
{
	adata->playerData = HashAlloc();
	adata->categories = HashAlloc();
	adata->upgrades = HashAlloc();
	adata->items = HashAlloc();
	adata->buyList = HashAlloc();

	LLInit(&adata->categoriesList);
	LLInit(&adata->upgradesList);
	LLInit(&adata->itemsList);

	LLInit(&adata->freqs);
	LLInit(&adata->purchasedUpgrades);
	LLInit(&adata->coolList);
}

static void DestructArenaData(struct arenaData *adata)
{
	struct freqData *fdata;
	Link *l;

	LLEmpty(&adata->categoriesList);
	LLEmpty(&adata->upgradesList);
	LLEmpty(&adata->itemsList);

	HashEnum(adata->playerData, CleanupPlayerData_HashEnum, NULL);
	HashEnum(adata->categories, DestructArenaData_HashEnum_categories, adata);
	HashEnum(adata->upgrades,   DestructArenaData_HashEnum_upgrades, adata);
	HashEnum(adata->items,      DestructArenaData_HashEnum_items, adata);
	HashEnum(adata->buyList,    hash_enum_afree, adata);

	HashFree(adata->playerData);
	HashFree(adata->categories);
	HashFree(adata->upgrades);
	HashFree(adata->items);
	HashFree(adata->buyList);

	adata->categories = NULL;
	adata->upgrades = NULL;
	adata->items = NULL;
	adata->buyList = NULL;

	for (l = LLGetHead(&adata->freqs); l; l = l->next)
	{
		fdata = l->data;
		DestructFreqData(fdata);
		tFREE(fdata);
	}
	LLEmpty(&adata->freqs);
}

#define tCLEANUPREQUIREMENTS(STRUCT) \
	for (l = LLGetHead(&STRUCT->config.requirements); l; l = l->next) \
	{ \
		freeRequirement(l->data); \
	} \
	LLEmpty(&STRUCT->config.requirements);

static void freeCategoryData(struct categoryData *category)
{
	Link *l;

	tFREE(category->config.name);
	tFREE(category->config.description);
	tFREE(category->config.freqs);

	tCLEANUPREQUIREMENTS(category)

	tFREE(category);
}

static void freeUpgradeData(struct upgradeData *upgrade)
{
	Link *l;

	if (upgrade->setter)
	{
		attrman->Lock();
		attrman->UnregisterSetter(upgrade->setter);
		attrman->UnLock();
	}
	tFREE(upgrade->config.name);
	tFREE(upgrade->config.description);
	tFREE(upgrade->config.freqs);

	if (upgrade->config.target == UPGRADETARGET_FREQ)
	{
		for (l = LLGetHead(&upgrade->partialPurchase.freqs); l ; l = l->next)
			tFREE(l->data);;

		LLEmpty(&upgrade->partialPurchase.freqs);
	}

	tCLEANUPREQUIREMENTS(upgrade)

	for (l = LLGetHead(&upgrade->config.attributes); l; l = l->next)
		tFREE(l->data);

	LLEmpty(&upgrade->config.attributes);

	for (l = LLGetHead(&upgrade->config.actions); l; l = l->next)
		freeAction(l->data);

	LLEmpty(&upgrade->config.actions);

	tFREE(upgrade);
}

static void freeItemData(struct itemData *item)
{
	Link *l;

	tFREE(item->config.name);
	tFREE(item->config.description);
	tFREE(item->config.freqs);

	tCLEANUPREQUIREMENTS(item)

	for (l = LLGetHead(&item->config.actions); l; l = l->next)
		freeAction(l->data);

	LLEmpty(&item->config.actions);

	tFREE(item);
}

static int DestructArenaData_HashEnum_categories (const char *key, void *val, void *clos)
{
	freeCategoryData(val);
	return 1;
}

static int DestructArenaData_HashEnum_upgrades (const char *key, void *val, void *clos)
{
	 freeUpgradeData(val);
	return 1;
}

static int DestructArenaData_HashEnum_items (const char *key, void *val, void *clos)
{
	freeItemData(val);
	return 1;
}


#undef tCLEANUPREQUIREMENTS

static struct arenaData *GetArenaData(Arena *arena)
{
	assertlm(arena);
	assertlm(arenaKey != -1);
	return P_ARENA_DATA(arena, arenaKey);
}

static void ConstructFreqData(struct freqData *fdata)
{
	assertlm(fdata);
	LLInit(&fdata->buyList);
	LLInit(&fdata->purchasedUpgrades);
	LLInit(&fdata->coolList);
}

static void DestructFreqData(struct freqData *fdata)
{
	assertlm(fdata);
	LLEmpty(&fdata->buyList);
	LLEmpty(&fdata->purchasedUpgrades);
	LLEnum(&fdata->coolList, afree);
	LLEmpty(&fdata->coolList);
}

static struct freqData *GetFreqData(Arena *arena, int freq, bool allocFreqData)
{
	struct arenaData *adata;
	struct freqData *fdata;
	Link *l;

	assertlm(arena);
	assertlm(freq < 10000);
	adata = GetArenaData(arena);
	assertlm(adata->initialized);


	for (l = LLGetHead(&adata->freqs); l; l = l->next)
	{
		fdata = l->data;

		if (fdata->freq == freq)
			return fdata;
	}

	// freq data not found after this point
	if (allocFreqData)
	{
		fdata = amalloc(sizeof(struct freqData));
		ConstructFreqData(fdata);
		fdata->arena = arena;
		fdata->freq = freq;

		LLAdd(&adata->freqs, fdata);
		return fdata;
	}

	return NULL;
}

static void ConstructPlayerData(struct playerData *pdata)
{
	assertlm(pdata);
	LLInit(&pdata->purchasedUpgrades);
	LLInit(&pdata->coolList);
}

static void DestructPlayerData(struct playerData *pdata)
{
	assertlm(pdata);
	LLEmpty(&pdata->purchasedUpgrades);
	LLEnum(&pdata->coolList, afree);
	LLEmpty(&pdata->coolList);
}

static struct playerData* GetPlayerData(Player *p)
{
	struct arenaData *adata;
	struct playerData *pdata;
	struct playerData_pd *pdata_pd;

	//p->arena may NOT be NULL in this function
	assertlm(p);
	if (!p->arena)
		return NULL;

	assertlm(p->name);

	pdata_pd = PPDATA(p, playerKey);
	pdata = pdata_pd->pdata;

	if (!pdata || pdata->arena != p->arena) // no player data found, or it was from an old arena
	{
		adata = GetArenaData(p->arena);
		pdata = HashGetOne(adata->playerData, p->name); // have we played in this arena before?

		if (!pdata) //no we haven't, create a new one
		{
			pdata = amalloc(sizeof(struct playerData));
			pdata->p = p;
			ConstructPlayerData(pdata);
			HashReplace(adata->playerData, p->name, pdata);
		}
		pdata_pd->pdata = pdata;
	}

	return pdata;
}



static void InitializeArena(Arena *arena)
{
	struct arenaData *adata;
	assertlm(arena);
	adata = GetArenaData(arena);
	assertlm(!adata->initialized);

	ConstructArenaData(adata);
	ReadConfig(arena);
}
/** Clean up everything so this module can be properly detached */
static void CleanupArena(Arena *arena)
{
	struct arenaData *adata = GetArenaData(arena);
	assertlm(arena);
	adata = GetArenaData(arena);
	assertlm(adata->initialized);
	adata->initialized = false;

	DestructArenaData(adata);
}

static void ReadConfig(Arena *arena)
{
	/*
		Create the keys into the hash table from buy:CategoryX, buy:UpgradeX and buy:ItemX;

		Loop through adata->categories, parse everything from [buy_category_KEY] \
			For every buy_category_KEY:Freq, add this pointer to freqData->categories;

		Loop through adata->upgrades, parse everything from [buy_upgrade_KEY] \
			For every buy_upgrade_KEY:Freq, add this pointer to freqData->upgrades;

		Loop through adata->items, parse everything from [buy_item_KEY] \
			For every buy_item_KEY:Freq, add this pointer to freqData->items;
	*/
	struct arenaData *adata;
	int a;
	const char *s;
	Link *l;
	char buf [BUF_CHARS+1];

	struct categoryData  *category;
	struct upgradeData   *upgrade;
	struct itemData      *item;

	ConfigHandle ch;

	assertlm(arena);
	adata = GetArenaData(arena);
	assertlm(!adata->initialized);

	adata->initialized = true;

	ch = arena->cfg;
	adata->minimumPartialPurchaseForAnnounce = cfg->GetInt(ch, "buy", "MinimumPartialPurchaseForAnnounce", 100);

	// Create the keys into the hash table from buy:CategoryX, buy:UpgradeX and buy:ItemX;

	for (a = 0; a < MAXIMUMCONFIGENTRIES; a++)
	{
		snprintf(buf, sizeof(buf), "Category%d", a);

		s = cfg->GetStr(ch, "buy", buf);

		if (!s) //reached end
		{
			lm->LogA(L_INFO, "advbuy", arena, "Reached end of category list with Category%d", a);
			break;
		}

		if (HashGetOne(adata->categories, s))
		{
			lm->LogA(L_WARN, "advbuy", arena, "Category (%s) already defined: buy:Category%d", s, a);
			continue;
		}

		category = amalloc(sizeof(struct categoryData));
		LLInit(&category->config.requirements);

		category->config.key = HashAdd(adata->categories, s, category);
		LLAdd(&adata->categoriesList, category);
		category = NULL;
	}

	for (a = 0; a < MAXIMUMCONFIGENTRIES; a++)
	{
		snprintf(buf, sizeof(buf), "Upgrade%d", a);

		s = cfg->GetStr(ch, "buy", buf);

		if (!s) //reached end
		{
			lm->LogA(L_INFO, "advbuy", arena, "Reached end of upgrade list with Upgrade%d", a);
			break;
		}

		if (HashGetOne(adata->upgrades, s)) //error: already exists.
		{
			lm->LogA(L_WARN, "advbuy", arena, "Upgrade (%s) already defined: buy:Upgrade%d", s, a);
			continue;
		}

		upgrade = amalloc(sizeof(struct upgradeData));
		LLInit(&upgrade->config.requirements);

		LLInit(&upgrade->config.attributes);
		LLInit(&upgrade->config.actions);

		upgrade->config.key = HashAdd(adata->upgrades, s, upgrade);
		LLAdd(&adata->upgradesList, upgrade);
		upgrade = NULL;
	}

	for (a = 0; a < MAXIMUMCONFIGENTRIES; a++)
	{
		snprintf(buf, sizeof(buf), "Item%d", a);

		s = cfg->GetStr(ch, "buy", buf);

		if (!s) //reached end
		{
			lm->LogA(L_INFO, "advbuy", arena, "Reached end of item list with Item%d", a);
			break;
		}

		if (HashGetOne(adata->items, s)) //error: already exists.
		{
			lm->LogA(L_WARN, "advbuy", arena, "Item (%s) already defined: buy:Item%d", s, a);
			continue;
		}

		item = amalloc(sizeof(struct itemData));
		LLInit(&item->config.requirements);
		LLInit(&item->config.actions);

		item->config.key = HashAdd(adata->items, s, item);
		LLAdd(&adata->itemsList, item);
		item = NULL;
	}

	// Second pass
	l = LLGetHead(&adata->categoriesList);
	while (l)
	{
		category = l->data;
		l = l->next;

		if (!
			 ReadConfig_category(arena, category)
		   )
		{ // error occured, remove it
			LLRemove(&adata->categoriesList, category);
			HashRemove(adata->categories, category->config.key, category);
			freeCategoryData(category);
		}
	}

	l = LLGetHead(&adata->upgradesList);
	while (l)
	{
		upgrade = l->data;
		l = l->next;
		if (!
			 ReadConfig_upgrade(arena, (struct upgradeData *) upgrade)
		   )
		{
			LLRemove(&adata->upgradesList, upgrade);
			HashRemove(adata->upgrades, upgrade->config.key, upgrade);
			freeUpgradeData(upgrade);
		}
	}

	l = LLGetHead(&adata->itemsList);
	while (l)
	{
		item = l->data;
		l = l->next;
		if (!
			 ReadConfig_item(arena, (struct itemData *) item)
		   )
		{
			LLRemove(&adata->itemsList, item);
			HashRemove(adata->items, item->config.key, item);
			freeItemData(item);
		}
	}
}

static void skipPastDelim(const char **str)
{
	const char *s = *str;
	while (*s && (*s == ' ' || *s == ','))
		s++;

	*str = s;
}

static char* strcpyalloc(const char *s)
{
	char *ret = NULL;
	int len;
	size_t size;

	if (s)
	{
		len = strlen(s) + 1; //including \0
		size = len * sizeof(char);
		if (size > 0)
		{
			ret = amalloc(size);
			astrncpy(ret, s, len);
		}
	}

	return ret;
}

static char* cfgGetStrAlloc(ConfigHandle ch, const char *section, const char *key)
{
	const char *s = cfg->GetStr(ch, section, key);

	return strcpyalloc(s);
}




// STRUCT must be "category" OR "upgrade" OR "item"
// BUYITEM must be BUYITEM_CATEGORY, BUYITEM_UPGRADE or BUYITEM_ITEM
// requires "struct categoryData *category" OR "struct upgradeData *upgrade" OR "struct itemData *item"
// requires "struct buyItemData *buyitem" AND "ConfigHandle ch" AND "struct arenaData *adata" AND buf to be filled
#define tPARSENAME(STRUCT, BUYITEM) \
	STRUCT->config.name = cfgGetStrAlloc(ch, buf, "Name"); \
	\
	if (STRUCT->config.name) \
	{ \
		\
		buyitem = amalloc(sizeof(struct buyItemData)); \
		buyitem->type = BUYITEM; \
		buyitem->u.STRUCT = STRUCT; \
		\
		HashAdd(adata->buyList, STRUCT->config.name, buyitem); \
	} \
	else \
	{ \
		lm->LogA(L_WARN, "advbuy", arena, "No name set for %s", STRUCT->config.key); \
		return false; \
	}


// requires "struct categoryData *category" OR "struct upgradeData *upgrade" OR "struct itemData *item"
// requires "const char *s" AND "char *s2" AND "long freq" AND "struct freqData *fdata" AND "ConfigHandle ch" AND buf to be filled
#define tPARSEFREQS(STRUCT) \
	s = cfg->GetStr(ch, buf, "Freqs"); \
	s2 = (char*) s;  \
	\
	\
	STRUCT->config.freq_length = 0; \
	while(s && *s) \
	{ \
		if (*s == ',' || *s == ' ' || *s == '\t') \
		{ \
			s++; \
			continue; \
		} \
		else \
		{ \
			freq = strtol(s, &s2, 0); \
			if (s == s2) \
			{ \
				s++; \
				continue; \
			} \
			\
			s = s2; \
			\
			if (freq < 0 || freq > 9999) \
				continue; \
			\
			STRUCT->config.freq_length++; \
			fdata = GetFreqData(arena, freq, true); \
			LLAdd(&fdata->buyList, buyitem); \
			buf_freqs[STRUCT->config.freq_length-1] = freq; \
		} \
		if (STRUCT->config.freq_length) STRUCT->config.freqs = amalloc(sizeof(short) * STRUCT->config.freq_length); \
		for (a = 0; a < STRUCT->config.freq_length; a++) \
			STRUCT->config.freqs[a] = buf_freqs[a]; \
	} \
	fdata = NULL;

// STRUCT must be "category", "upgrade" or "item"
// requires "struct categoryData *category" OR "struct upgradeData *upgrade" OR "struct itemData *item"
// requires "struct requirementData *requirement" AND "const char *s" AND "ConfigHandle ch" AND "enum REQUIREMENTTYPE requirementType" AND buf to be filled
#define tPARSEREQUIREMENTS(STRUCT) \
	LLInit(&STRUCT->config.requirements); \
	for (a = 0; a < MAXIMUMCONFIGENTRIES; a++) \
	{ \
		snprintf(buf2, sizeof(buf2), "Requirement%d", a); \
		requirementType = cfgval_requirementType(cfg->GetStr(ch, buf, buf2), REQUIREMENT_UNSET); \
		\
		if (requirementType <= REQUIREMENT_UNSET) break; \
		if (requirementType == REQUIREMENT_NONE || requirementType >= REQUIREMENTTYPE_COUNT) continue; \
		if (!USE_STRUCTURES_BOOL && (requirementType == REQUIREMENT_STRUCTURETYPE || requirementType == REQUIREMENT_STRUCTURE)) continue; \
		if (!USE_SUPERWEAPON_BOOL && (requirementType == REQUIREMENT_SUPERWEAPONCHARGE)) continue; \
		\
		requirement = amalloc(sizeof(struct requirementData)); \
		requirement->requirementType = requirementType; \
		OK = true; \
		\
		if (requirement->requirementType != REQUIREMENT_SAFEZONE) \
		{ \
			snprintf(buf2, sizeof(buf2), "RequirementValue%d", a); \
			\
			s = cfg->GetStr(ch, buf, buf2); \
			OK = false; \
			\
			if (s) \
			{ \
				switch (requirement->requirementType)\
				{\
					case REQUIREMENT_UPGRADE: \
						requirement->value.upgrade.upgrade = HashGetOne(adata->upgrades, s); \
						if (requirement->value.upgrade.upgrade) OK = true; \
						break; \
					case REQUIREMENT_STRUCTURETYPE: \
						requirement->value.structuretype.type = cfg->GetInt(ch, buf, buf2, -1); \
						if (requirement->value.structuretype.type >= 0 && requirement->value.structuretype.type < STRUCTURES_STRUCTURETYPES) \
							OK = true; \
						break; \
					case REQUIREMENT_STRUCTURE: \
						sscanf(s, "%511s", buf3); \
						requirement->value.structure.structure = strcpyalloc((const char*)buf3); \
						if (requirement->value.structure.structure) OK = true; \
						break; \
					case REQUIREMENT_SHIP: \
						requirement->value.ship.ship = 0; \
						int ship = 0; \
						do \
						{ \
							if (!*s) break; \
							ship = strtol(s, (char**)&s, 10); \
							if (ship > 0 && ship <= 32) \
							{ \
								requirement->value.ship.ship |= 1 << (ship-1); \
								skipPastDelim(&s); \
							} \
						} while (ship > 0); \
						\
						if (requirement->value.ship.ship) \
							OK = true; \
						break; \
					case REQUIREMENT_SAFEZONE: \
						OK = true; \
						break; \
					case REQUIREMENT_REGION: \
						requirement->value.region.region = map->FindRegionByName(arena, s); \
						if (requirement->value.region.region) \
						{ \
							OK = true; \
						} \
						else \
						{ \
							lm->LogA(L_WARN, "advbuy", arena, "Unknown region %s", s); \
						} \
						break; \
					case REQUIREMENT_SUPERWEAPONCHARGE: \
						sscanf(s, "%d,%d,%d", (int*)&requirement->value.superweaponcharge.superweapon, \
								      &requirement->value.superweaponcharge.freqCharge, \
								      &requirement->value.superweaponcharge.playerCharge); \
						if (requirement->value.superweaponcharge.superweapon >= 0 && requirement->value.superweaponcharge.superweapon < SUPERWEAPONS) \
							OK = true; \
						break; \
					default: \
						assertlm(false); \
						break; \
				} \
			} \
			else \
			{ \
				lm->LogA(L_WARN, "advbuy", arena, "Missing requirement value for %s:%s", buf, buf2); \
				tFREE(requirement); \
				OK = false; \
			} \
		} \
		\
		if (OK) \
			LLAdd(&STRUCT->config.requirements, requirement); \
		else \
		{ \
			tFREE(requirement); \
			lm->LogA(L_WARN, "advbuy", arena, "Invalid value for %s:%s", buf, buf2); \
		} \
	} \
	requirement = NULL;

// STRUCT must be "upgrade" or "item"
// requires "struct upgradeData *upgrade" OR "struct itemData *item"
// requires "enum ITEMACTION itemaction" OR "struct actionData *action"
// requires "ConfigHandle ch" AND "const char *s" AND buf to be filled
#define tPARSEACTIONS(STRUCT) \
	 LLInit(&STRUCT->config.actions); \
	for (a = 0; a < MAXIMUMCONFIGENTRIES; a++) \
	{ \
		snprintf(buf2, sizeof(buf2), "Action%d", a); \
		itemaction = cfgval_itemAction(cfg->GetStr(ch, buf, buf2), ITEMACTION_UNSET); \
		\
		if (itemaction < 0) break; \
		if (itemaction == ITEMACTION_NONE || itemaction >= ITEMACTION_COUNT) continue; \
		if (!USE_STRUCTURES_BOOL && itemaction == ITEMACTION_SPAWNSTRUCTURE) continue; \
		if (!USE_STATICTURRET_BOOL && itemaction == ITEMACTION_STATICTURRET) continue; \
		if (!USE_SUPERWEAPON_BOOL && itemaction == ITEMACTION_NUKE) continue; \
		\
		action = amalloc(sizeof(struct actionData)); \
		action->action = itemaction; \
		\
		snprintf(buf2, sizeof(buf2), "ActionValue%d", a); \
		\
		s = cfg->GetStr(ch, buf, buf2); \
		\
		switch (action->action) \
		{ \
			case ITEMACTION_PRIZE: \
				action->value.prize.type = 0; \
				action->value.prize.count = 1; \
				if (s) sscanf(s, "%d,%d", &action->value.prize.type, &action->value.prize.count); \
				\
				break; \
			case ITEMACTION_STATICTURRET: \
				action->value.staticturret.key = strcpyalloc(s); \
				break; \
			case ITEMACTION_SPAWNSTRUCTURE: \
				action->value.spawnstructure.key = strcpyalloc(s); \
				break; \
			case ITEMACTION_NUKE: \
				action->value.nuke.refundCredits = 0; \
				action->value.nuke.freqChargeRequired = 0; \
				if (s) sscanf(s, "%d,%d", &action->value.nuke.refundCredits, &action->value.nuke.freqChargeRequired); \
				break; \
			default: \
				assertlm(false); \
				break; \
		} \
		\
		LLAdd(&STRUCT->config.actions, action); \
	} \
	action = NULL;

static bool ReadConfig_category(Arena *arena, struct categoryData *category)
{
	//Read every setting from [buy_category_KEY]
	bool OK;
	struct arenaData *adata;

	struct buyItemData *buyitem;
	ConfigHandle ch = arena->cfg;
	struct freqData *fdata;
	const char *s;
	int a;
	char *s2;
	enum REQUIREMENTTYPE requirementType;
	long freq;
	struct requirementData *requirement;
	char buf [BUF_CHARS+1];
	char buf2[BUF_CHARS+1];
	char buf3[BUF_CHARS+1];
	short buf_freqs[255];

	assertlm(arena);
	adata = GetArenaData(arena);

	snprintf(buf, sizeof(buf), "buy_category_%s", category->config.key);

	tPARSENAME(category, BUYITEM_CATEGORY)

	category->config.description  = cfgGetStrAlloc(ch, buf, "Description");
	category->config.staffOnly = cfgval_bool(cfg->GetStr(ch, buf, "StaffOnly"), false);


	category->config.parentCategory = NULL;
	s = cfg->GetStr(ch, buf, "ParentCategory");
	if (s)
	{
		category->config.parentCategory = HashGetOne(adata->categories, s);
	}


	tPARSEFREQS(category)

	tPARSEREQUIREMENTS(category)

	return true;
}

static bool ReadConfig_upgrade(Arena *arena, struct upgradeData *upgrade)
{
	//Read every setting from [buy_upgrade_KEY]
	struct arenaData *adata;
	bool OK;
	struct buyItemData *buyitem;
	const char *s;
	char *s2;
	long freq;
	struct freqData *fdata;
	int a;
	enum REQUIREMENTTYPE requirementType;
	struct requirementData *requirement;
	ConfigHandle ch = arena->cfg;
	enum ITEMACTION itemaction;
	struct actionData *action;
	struct attributeData *attr;
	ATTRIBUTESTRING_OPERATION operation;
	char buf [BUF_CHARS+1];
	char buf2[BUF_CHARS+1];
	char buf3[BUF_CHARS+1];
	short buf_freqs[255];

	adata = GetArenaData(arena);

	snprintf(buf, sizeof(buf), "buy_upgrade_%s", upgrade->config.key);

	tPARSENAME(upgrade, BUYITEM_UPGRADE)

	upgrade->setter = 0;
	upgrade->config.description = cfgGetStrAlloc(ch, buf, "Description");
	upgrade->config.target = cfgval_upgradeTarget(cfg->GetStr(ch, buf, "Target"), UPGRADETARGET_PLAYER);
	upgrade->config.cost = cfg->GetInt(ch, buf, "Cost", 0);
	upgrade->config.staffOnly = cfgval_bool(cfg->GetStr(ch, buf, "StaffOnly"), false);

	if (upgrade->config.target == UPGRADETARGET_FREQ)
		LLInit(&upgrade->partialPurchase.freqs);

	MIN(upgrade->config.cost, 0);

	if (!(upgrade->config.target == UPGRADETARGET_PLAYER 
		|| upgrade->config.target == UPGRADETARGET_FREQ 
		|| upgrade->config.target == UPGRADETARGET_ARENA))
	{
		lm->LogA(L_WARN, "advbuy", arena, "Wrong value for %s:Target", buf);
		upgrade->config.target = UPGRADETARGET_PLAYER;
	}

	upgrade->config.category = NULL;
	s = cfg->GetStr(ch, buf, "Category");
	if (s)
	{
		upgrade->config.category = HashGetOne(adata->categories, s);
	}


	for (a = 0; a < MAXIMUMCONFIGENTRIES; a++)
	{
		snprintf(buf2, sizeof(buf2), "Attribute%d", a);

		s = cfg->GetStr(ch, buf, buf2);
		if (!s) break;

		attr = amalloc(sizeof(struct attributeData));
		if (attrman->parseAttributeString(s, buf2, BUF_CHARS, &attr->value, &attr->ship, &operation) 
			&& operation != ATTRMAN_OP_UNSET 
			&& operation != ATTRMAN_OP_NOTHING)
		{
			attr->property = HashGetOne(usedAttributes, buf2);
			if (!attr->property) // Attribuut nog nooit eerder gebruikt, maak een nieuwe string
			{
				attr->property = HashAdd(usedAttributes, buf2, NULL);
			}

			attr->addition = operation == ATTRMAN_OP_ADDITION;
		}
		else
		{
			tFREE(attr);;
			lm->LogA(L_WARN, "advbuy", arena, "Wrong value for %s:%s", buf, buf2);
		}

		LLAdd(&upgrade->config.attributes, attr);
	}

	tPARSEFREQS(upgrade)

	tPARSEREQUIREMENTS(upgrade)

	tPARSEACTIONS(upgrade)

	return true;
}

static bool ReadConfig_item(Arena *arena, struct itemData *item)
{
	//Read every setting from [buy_item_KEY]
	struct arenaData *adata;
	struct buyItemData *buyitem;
	struct freqData *fdata;
	const char *s;
	char *s2;
	long freq;
	bool OK;
	int a;
	enum REQUIREMENTTYPE requirementType;
	enum ITEMACTION itemaction;
	struct requirementData *requirement;
	struct actionData *action;
	ConfigHandle ch = arena->cfg;
	char buf [BUF_CHARS+1];
	char buf2[BUF_CHARS+1];
	char buf3[BUF_CHARS+1];
	short buf_freqs[255];

	adata = GetArenaData(arena);

	snprintf(buf, sizeof(buf), "buy_item_%s", item->config.key);


	tPARSENAME(item, BUYITEM_ITEM)

	item->config.description = cfgGetStrAlloc(ch, buf, "Description");
	item->config.cost = cfg->GetInt(ch, buf, "Cost", 0);
	item->config.staffOnly = cfgval_bool(cfg->GetStr(ch, buf, "StaffOnly"), false);
	item->config.arenaCool = cfg->GetInt(ch, buf, "ArenaCool", 0);
	item->config.freqCool = cfg->GetInt(ch, buf, "FreqCool", 0);
	item->config.playerCool = cfg->GetInt(ch, buf, "PlayerCool", 0);

	MIN(item->config.cost, 0);

	item->config.category = NULL;
	s = cfg->GetStr(ch, buf, "Category");
	if (s)
	{
		item->config.category = HashGetOne(adata->categories, s);
	}

	tPARSEFREQS(item)

	tPARSEREQUIREMENTS(item)

	tPARSEACTIONS(item)

	return true;
}

#undef tPARSENAME
#undef tPARSEFREQS
#undef tPARSEREQUIREMENTS

static void PlayerActionCB(Player *p, int action, Arena *arena)
{
	struct arenaData *adata;
	struct playerData *pdata;
	Target scope;
	Link *l;
	scope.type = T_PLAYER;
	scope.u.p = p;

	if (action == PA_ENTERARENA)
	{
		// Give the player his upgrade back

		adata = GetArenaData(arena);
		if (!adata->running)
			return;

		pdata = GetPlayerData(p);
		pdata->p = p;
		for (l = LLGetHead(&pdata->purchasedUpgrades); l; l = l->next)
		{
			SetUpgradeAttributes(&scope, (struct upgradeData *) l->data);
		}
	}
	else if (action == PA_LEAVEARENA)
	{
		adata = GetArenaData(arena);
		if (!adata->running)
			return;

		pdata = GetPlayerData(p);
		pdata->p = NULL; // very important; player pointer may be free'd after this

		// no need to unset attributes on arena leave, attrman already does this
	}
}

static void ArenaAction(Arena *arena, int action)
{
	struct arenaData *adata;
	if (action == AA_CREATE && arena->status < ARENA_CLOSING)
	{
		adata = GetArenaData(arena);

		if (!adata->initialized)
			InitializeArena(arena);
	}
	else if (action == AA_DESTROY)
	{
		adata = GetArenaData(arena);

		if (adata->initialized)
			CleanupArena(arena);
	}
}

static int StopGame_HashEnum_upgrades(const char *key, void *val, void *clos)
{
	Arena *arena = clos;
	struct upgradeData *upgrade = val;
	assertlm(arena);
	assertlm(upgrade);

	RemoveUpgradeEverywhere(arena, upgrade);
	return 0; // dont remove it
}

static bool StartGame(Arena *arena)
{
	struct arenaData *adata = GetArenaData(arena);
#ifdef USE_SUPERWEAPON
	Isuperweapon *superweapon;
#endif
	
	if (!adata->initialized)
		return false;

	StopGame(arena); // Make sure it is stopped first

	adata->running = true;

#ifdef USE_SUPERWEAPON
	if ( (superweapon = mm->GetInterface(I_SUPERWEAPON, arena)) )
		superweapon->StartGame(arena);
	mm->ReleaseInterface(superweapon);
#endif
	return true;
}

static void StopGame(Arena *arena)
{
	// End of the game
#ifdef USE_SUPERWEAPON
	Isuperweapon *superweapon;
#endif
	struct arenaData *adata = GetArenaData(arena);
	adata->running = false;

#ifdef USE_SUPERWEAPON
	if ( (superweapon = mm->GetInterface(I_SUPERWEAPON, arena)) )
		superweapon->StopGame(arena);
	mm->ReleaseInterface(superweapon);
#endif

	if (adata->upgrades)
		HashEnum(adata->upgrades, StopGame_HashEnum_upgrades, arena);
}

//000000000111111111122222222223333333333444444444455555555556666666666777777777788888888889999999999
//123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
//--------------------------------

//Sends all the available categories to the player
static unsigned int ListCategories(Player *p, struct freqData *fdata, struct categoryData *parentCategory)
{
	Arena *arena;
	struct buyItemData *buyitem;
	struct categoryData *category;
	bool buystaffitemsCap;
	Link *l;
	unsigned int a = 0;
	enum REQUIREMENTTYPE failedRequirement;
	bool requirementMet;
	int messageType;

	assertlm(p);
	arena = p->arena;
	assertlm(fdata);

	assertlm(arena);
	assertlm(fdata->arena == arena);

	LinkedList pLL = LL_INITIALIZER; //linked list pointing to 1 player
	LLAdd(&pLL, p);

	buystaffitemsCap = capman->HasCapability(p, CAP_BUYSTAFFITEMS);



#define tSENDHEADER(p) \
	if (parentCategory) \
	{ \
	chat->SendMessage(p, "+----------------------------------+"); \
	chat->SendMessage(p, "| %-32s |", parentCategory->config.name); \
	chat->SendMessage(p, "+----------------------------------+--------------------------------------------------------------------+"); \
	} \
	else \
	{ \
	chat->SendMessage(p, "+----------------------------------+--------------------------------------------------------------------+"); \
	} \
	chat->SendMessage(p, "| Category Name                    | Category                                                           |"); \
	chat->SendMessage(p, "+----------------------------------+--------------------------------------------------------------------+")

#define tSENDFOOTER(p) \
	chat->SendMessage(p, "+----------------------------------+--------------------------------------------------------------------+")


	for (l = LLGetHead(&fdata->buyList); l; l = l->next)
	{
		buyitem = l->data;
		if (buyitem->type != BUYITEM_CATEGORY) continue;
		category = buyitem->u.category;

		if (category->config.parentCategory != parentCategory)
			continue;

		requirementMet = RequirementsMetCategory(p, category, &failedRequirement, false);


		if (requirementMet || !failedRequirementHides(failedRequirement))
		{
			if (requirementMet)
				messageType = MSG_ARENA;
			else
				messageType = MSG_SYSOPWARNING;

			if (!category->config.staffOnly)
			{
				if (!a) //first item
				{
					tSENDHEADER(p);
				}

				chat->SendAnyMessage(&pLL, messageType, 0, NULL, "| %-32s | %-66s |", category->config.name, category->config.description ? category->config.description : "");
				a++;
			}
			else if (buystaffitemsCap)
			{
				if (!a) //first item
				{
					tSENDHEADER(p);
				}
				chat->SendAnyMessage(&pLL, messageType, 0, NULL, "| (%-30s) | %-66s |", category->config.name, category->config.description ? category->config.description : "");
				a++;
			}
		}
	}

	if (a) //not empty
	{
		tSENDFOOTER(p);
	}
	LLEmpty(&pLL);
	return a;

#undef tSENDHEADER
#undef tSENDFOOTER
}


//Sends all the available items and upgrades inside a category to the player
static unsigned int ListItems(Player *p, struct freqData *fdata, struct categoryData *category, bool outputHeader)
{
	Arena *arena = p->arena;
	struct upgradeData *upgrade;
	struct itemData *item;
	struct buyItemData *buyitem;
	int messageType;
	unsigned int a;
	Link *l;
	bool buystaffitemsCap;
	long creds;
	enum REQUIREMENTTYPE failedRequirement;
	struct arenaData *adata;
	bool requirementMet;

	assertlm(p);
	assertlm(fdata);

	arena = p->arena;
	assertlm(arena);
	assertlm(fdata->arena == arena);

	LinkedList pLL = LL_INITIALIZER; //linked list pointing to 1 player
	LLAdd(&pLL, p);

	adata = GetArenaData(arena);

	buystaffitemsCap = capman->HasCapability(p, CAP_BUYSTAFFITEMS);
	creds = adata->credits->GetCredits(p, p->p_freq);


#define tSENDHEADER(p, category) \
	if (category && outputHeader) \
	{ \
	chat->SendMessage(p, "+----------------------------------+"); \
	chat->SendMessage(p, "| %-32s |", category->config.name); \
	chat->SendMessage(p, "+------------------------+---------+-+------------------------------------------------------------------+"); \
	} \
	else \
	{ \
	chat->SendMessage(p, "+------------------------+-----------+------------------------------------------------------------------+"); \
	} \
	chat->SendMessage(p, "| Item Name              | Cost      | Item Description                                                 |"); \
	chat->SendMessage(p, "+------------------------+-----------+------------------------------------------------------------------+")


#define tSENDFOOTER(p) \
	chat->SendMessage(p, "+------------------------+-----------+------------------------------------------------------------------+")


	if (category && category->config.staffOnly && !buystaffitemsCap)
	{
		return 0;
	}

	a = 0;
	for (l = LLGetHead(&fdata->buyList); l; l = l->next)
	{
		buyitem = l->data;


		if (buyitem->type == BUYITEM_UPGRADE)
		{
			upgrade = buyitem->u.upgrade;

			requirementMet = RequirementsMetUpgrade(p, upgrade, &failedRequirement, false);

			if ((requirementMet || !failedRequirementHides(failedRequirement)) &&
			    upgrade->config.category == category &&
			    !HasUpgrade(p, upgrade))
			{
				if (!requirementMet || creds < upgrade->config.cost)
				{
					messageType = MSG_SYSOPWARNING;
				}
				else
				{
					messageType = MSG_ARENA;
				}

				if (upgrade->config.staffOnly)
				{
					if (buystaffitemsCap)
					{
						if (!a)
						{
							tSENDHEADER(p, category);
						}

						chat->SendAnyMessage(&pLL, messageType, 0, NULL, "| (%-20s) | %-9ld | %-64s |", upgrade->config.name, GetRemainingCost(p, upgrade), upgrade->config.description ? upgrade->config.description : "");

						a++;
					}
				}
				else
				{
					if (!a)
					{
						tSENDHEADER(p, category);
					}

					chat->SendAnyMessage(&pLL, messageType, 0, NULL, "| %-22s | %-9ld | %-64s |", upgrade->config.name, GetRemainingCost(p, upgrade), upgrade->config.description ? upgrade->config.description : "");

					a++;
				}
			}
		}
		else if (buyitem->type == BUYITEM_ITEM)
		{
			item = buyitem->u.item;

			requirementMet = RequirementsMetItem(p, item, &failedRequirement, false);

			if ((requirementMet || !failedRequirementHides(failedRequirement)) &&
			    item->config.category == category)
			{
				if (!requirementMet || creds < item->config.cost)
				{
					messageType = MSG_SYSOPWARNING;
				}
				else
				{
					messageType = MSG_ARENA;
				}

				if (item->config.staffOnly)
				{
					if (buystaffitemsCap)
					{
						if (!a)
						{
							tSENDHEADER(p, category);
						}

						chat->SendAnyMessage(&pLL, messageType, 0, NULL, "| (%-20s) | %-9ld | %-64s |", item->config.name, item->config.cost, item->config.description ? item->config.description : "");

						a++;
					}
				}
				else
				{
					if (!a)
					{
						tSENDHEADER(p, category);
					}

					chat->SendAnyMessage(&pLL, messageType, 0, NULL, "| %-22s | %-9ld | %-64s |", item->config.name, item->config.cost, item->config.description ? item->config.description : "");

					a++;
				}
			}
		}
	}

	if (a)
	{
		tSENDFOOTER(p);
	}

	LLEmpty(&pLL);

	return a;
#undef tSENDHEADER
#undef tSENDFOOTER
}

// buyer may be null (this happends when the action is part of an upgrade, since upgrades can be activated not just on the moment it was ?buy'd)
static bool DoItemAction(Player *buyer, Target *tgt, struct actionData *action)
{
#ifdef USE_STATICTURRET
	enum ADDBOT_ERROR addBotError;
	Istaticturret *staticturret;
#endif

#ifdef USE_STRUCTURES
	Istructures *structures;
	structureData *sdata;
#endif

#ifdef USE_SUPERWEAPON
	Isuperweapon *superweapon;
#endif
	Arena *arena;

	if (tgt->type == T_PLAYER)
		arena = tgt->u.p->arena;
	else if (tgt->type == T_FREQ)
		arena = tgt->u.freq.arena;
	else if (tgt->type == T_ARENA)
		arena = tgt->u.arena;
	else
		assertlm(false);

	assertlm(action);
	switch (action->action)
	{
		case ITEMACTION_NONE:
			break;
		case ITEMACTION_PRIZE:
			assertlm(tgt);
			game->GivePrize(tgt, action->value.prize.type, action->value.prize.count);
			break;
#ifdef USE_STATICTURRET
		case ITEMACTION_STATICTURRET:
			if (!buyer) return false;

			staticturret = mm->GetInterface(I_STATICTURRET, arena);
			if (!staticturret)
			{
				lm->LogP(L_WARN, "<advbuy>", buyer, "Tried to purchase item, but staticturret is not attached / loaded");
				chat->SendCmdMessage(buyer, "Error while purchasing turret");

				mm->ReleaseInterface(staticturret);
				return false;
			}

			addBotError = staticturret->AddBot(arena, action->value.staticturret.key, buyer->position.x, buyer->position.y, buyer->p_freq, false, false);
			switch (addBotError)
			{
				case ADDBOT_OK:
				break;
				case ADDBOT_ILLEGAL_ARENA:
					lm->LogP(L_WARN, "<advbuy>", buyer, "Tried to purchase item, but staticturret is not attached / loaded");
					chat->SendCmdMessage(buyer, "Error while purchasing turret");
				break;
				case ADDBOT_UNKNOWN_TYPE:
					lm->LogP(L_WARN, "<advbuy>", buyer, "Tried to purchase item, but staticturret type %s is not recognised", action->value.staticturret.key);
					chat->SendCmdMessage(buyer, "Error while purchasing turret");
				break;
				case ADDBOT_MAX_REACHED_FOR_BOTTYPE:
					chat->SendCmdMessage(buyer, "There are no available bots for that turret type");
				break;
				case ADDBOT_MAX_REACHED_FOR_ARENA:
					chat->SendCmdMessage(buyer, "There are no available bots for this arena");
				break;
				case ADDBOT_CAN_NOT_BE_PLACED_ON_MAP:
					chat->SendCmdMessage(buyer, "Turret can not be placed there");
				break;
				case ADDBOT_TO_CLOSE_TO_OTHER_BOT:
					chat->SendCmdMessage(buyer, "Turret is to close to another turret");
				break;
				case ADDBOT_BUILDING_IN_PROGRESS:
					chat->SendCmdMessage(buyer, "Unable to comply, building in progress");
				break;
				default:
					chat->SendCmdMessage(buyer, "Unknown error");
			}
			mm->ReleaseInterface(staticturret);
			if (addBotError != ADDBOT_OK) return false;

			break;
#endif
#ifdef USE_STRUCTURES
		case ITEMACTION_SPAWNSTRUCTURE:
			structures = mm->GetInterface(I_STRUCTURES, arena);
			if (!structures)
			{
				lm->LogA(L_WARN, "<advbuy>", arena, "Someone tried to purchase item, but structures is not attached / loaded");

				mm->ReleaseInterface(structures);
				return false;
			}

			sdata = structures->GetStructureData(arena, action->value.spawnstructure.key);
			if (!sdata)
			{
				lm->LogA(L_WARN, "<advbuy>", arena, "Unknown structure %s while purchasing an item", action->value.spawnstructure.key);
			}

			if (!sdata->alive)
				structures->SpawnStructure(sdata, false);
			mm->ReleaseInterface(structures);
			break;
#endif
#ifdef USE_SUPERWEAPON
		case ITEMACTION_NUKE:
			if (!buyer) break; // not supported
			superweapon = mm->GetInterface(I_SUPERWEAPON, arena);
			if (!superweapon)
			{
				lm->LogA(L_WARN, "<advbuy>", arena, "Someone tried to purchase item, but superweapon is not attached / loaded");

				mm->ReleaseInterface(superweapon);
				return false;
			}
			//
			superweapon->NukeFireSequence(buyer, action->value.nuke.refundCredits, action->value.nuke.freqChargeRequired);
			mm->ReleaseInterface(superweapon);
			break;
#endif
		default:
			lm->Log(L_WARN, "<advbuy> Unknown item action %d", action->action);
	}

	return true;
}

struct coolData* GetCoolData(LinkedList *coolList, struct itemData *item)
{
	Link *l;
	struct coolData* cool;
	for (l = LLGetHead(coolList); l; l = l->next)
	{
		cool = (struct coolData*) l->data;
		if (cool->item == item)
			return cool;
	}
	return NULL;
}

//buy an item
static void BuyItem(Player *p, struct itemData *item)
{
	long creds;
	struct actionData *action;
	Link *l;
	Target tgt;
	int largestCool;
	int secs;
	struct coolData *arenaCool = NULL, *freqCool = NULL, *playerCool = NULL;
	int diff;
	ticks_t now;
	enum REQUIREMENTTYPE failedRequirement;
	struct arenaData *adata;
	struct playerData *pdata;
	struct freqData *fdata;

	adata = GetArenaData(p->arena);
	pdata = GetPlayerData(p);
	fdata = GetFreqData(p->arena, p->p_freq, true);
	now = current_ticks();

	if (!RequirementsMetItem(p, item, &failedRequirement, true))
	{
		return;
	}

	largestCool = 0;

	if (item->config.arenaCool)
	{
		arenaCool = GetCoolData(&adata->coolList, item);
		if (arenaCool)
		{
			diff = item->config.arenaCool - TICK_DIFF(now, arenaCool->lastUse);

			if (diff > largestCool)
				largestCool = diff;
		}
	}

	if (item->config.freqCool)
	{
		freqCool = GetCoolData(&fdata->coolList, item);
		if (freqCool)
		{
			diff = item->config.freqCool - TICK_DIFF(now, freqCool->lastUse);
			if (diff > largestCool)
				largestCool = diff;
		}
	}

	if (item->config.playerCool)
	{
		playerCool = GetCoolData(&pdata->coolList, item);
		if (playerCool)
		{
			diff = item->config.playerCool - TICK_DIFF(now, playerCool->lastUse);
			if (diff > largestCool)
				largestCool = diff;
		}
	}

	if (largestCool)
	{
		secs = largestCool / 100;
		if (largestCool && !secs)
			secs = 1;

		chat->SendCmdMessage(p, "Item %s has not recharged yet, please wait %d second%s", item->config.name, secs, secs == 1 ? "" : "s");
		return;
	}

	creds = adata->credits->GetCredits(p, p->p_freq);

	assertlm(item->config.cost >= 0);
	if (item->config.cost > creds)
	{
		chat->SendCmdMessage(p, "You do not have enough credits to buy item %s. You need %ld more.", item->config.name, item->config.cost - creds);
		return;
	}


	tgt.type = T_PLAYER;
	tgt.u.p = p;

	bool failed = false;
	for (l = LLGetHead(&item->config.actions); l; l = l->next)
	{
		action = l->data;
		if (!DoItemAction(p, &tgt, action)) // doitemaction should send a reason to the player if it failed
		{
			//refund money
			failed = true;
			break;
		}
	}

	if (!failed)
	{
		adata->credits->AddCreditsPlayer(p, p->p_freq, -item->config.cost, false, false);
		chat->SendCmdMessage(p, "Purchased item %s for %ld credits.", item->config.name, item->config.cost);

		if (item->config.arenaCool)
		{
			if (!arenaCool)
			{
				arenaCool = amalloc(sizeof(struct coolData));
				arenaCool->item = item;
				LLAdd(&adata->coolList, arenaCool);
			}
			arenaCool->lastUse = now;

		}

		if (item->config.freqCool)
		{
			if (!freqCool)
			{
				freqCool = amalloc(sizeof(struct coolData));
				freqCool->item = item;
				LLAdd(&fdata->coolList, freqCool);
			}
			freqCool->lastUse = now;

		}

		if (item->config.playerCool)
		{
			if (!playerCool)
			{
				playerCool = amalloc(sizeof(struct coolData));
				playerCool->item = item;
				LLAdd(&pdata->coolList, playerCool);
			}
			playerCool->lastUse = now;
		}
	}


	action = NULL;
}

static long GetRemainingCost(Player *p, struct upgradeData *upgrade)
{
	long cost;
	Link *link;
	struct partialFreqPurchase *freqPurchase;

	cost = upgrade->config.cost;

	if (upgrade->config.target == UPGRADETARGET_FREQ)
	{
		for (link = LLGetHead(&upgrade->partialPurchase.freqs); link; link = link->next)
		{
			freqPurchase = (struct partialFreqPurchase *) link->data;
			if (freqPurchase->fdata->freq == p->p_freq) // found it
			{
				cost -= freqPurchase->credits;
				break;
			}

		}
	}
	else if (upgrade->config.target == UPGRADETARGET_ARENA)
	{
		cost -= upgrade->partialPurchase.arena_credits;


	}

	return cost;
}

//buy an upgrade
static void BuyUpgrade(Player *p, struct upgradeData *upgrade)
{
	Arena *arena = p->arena;
	long creds, cost;
	Target scope;
	enum REQUIREMENTTYPE failedRequirement;
	struct arenaData *adata;
	LinkedList pLL = LL_INITIALIZER;
	LinkedList p2LL = LL_INITIALIZER;
	Player *q;
	Link *link;
	struct freqData *fdata;
	struct partialFreqPurchase *freqPurchase;

	assertlm(arena);
	assertlm(upgrade);

	adata = GetArenaData(arena);

	if (HasUpgrade(p, upgrade))
	{
		chat->SendCmdMessage(p, "You already have the upgrade %s.", upgrade->config.name);
		return;
	}

	if (!RequirementsMetUpgrade(p, upgrade, &failedRequirement, true))
	{
		return;
	}

	creds = adata->credits->GetCredits(p, p->p_freq);
	cost = upgrade->config.cost;
	assertlm(cost >= 0);

	if (!upgrade->setter)
	{
		attrman->Lock();
		upgrade->setter = attrman->RegisterSetter();
		attrman->UnLock();
	}
	if (upgrade->config.target == UPGRADETARGET_PLAYER)
	{
		if (cost > creds)
		{
			chat->SendCmdMessage(p, "You do not have enough credits to buy upgrade %s. You need %ld more.", upgrade->config.name, upgrade->config.cost - creds);
			return;
		}
		adata->credits->AddCreditsPlayer(p, p->p_freq, -cost, false, false);
		chat->SendCmdMessage(p, "Purchased upgrade %s for %ld credits.", upgrade->config.name, cost);

		scope.type = T_PLAYER;
		scope.u.p = p;

		struct playerData *pdata = GetPlayerData(p);
		LLAdd(&pdata->purchasedUpgrades, upgrade);
	}
	else if (upgrade->config.target == UPGRADETARGET_FREQ)
	{
		pd->Lock();
		FOR_EACH_PLAYER(q)
		{
			if (q->arena == arena && q->p_freq == p->p_freq && q != p) // players that receive an arena message about the upgrade
			{
				if (q->type == T_CONT)
					LLAdd(&pLL, q);
				else
					LLAdd(&p2LL, q);
			}
		}
		pd->Unlock();

		freqPurchase = NULL;
		for (link = LLGetHead(&upgrade->partialPurchase.freqs); link; link = link->next)
		{
			freqPurchase = (struct partialFreqPurchase *) link->data;
			if (freqPurchase->fdata->freq == p->p_freq) // found it
				break;

			freqPurchase = NULL;
		}

		if (freqPurchase)
			cost -= freqPurchase->credits;

		if (cost > creds)
		{
			if (!freqPurchase)
			{
				freqPurchase = amalloc(sizeof(struct partialFreqPurchase));
				freqPurchase->fdata = GetFreqData(arena, p->p_freq, true);
				LLAdd(&upgrade->partialPurchase.freqs, freqPurchase);
			}

			freqPurchase->credits += creds;
			cost -= creds;

			adata->credits->AddCreditsPlayer(p, p->p_freq, -creds, false, false);

			chat->SendCmdMessage(p, "Spent %ld credits on upgrade %s. %ld more credits are needed to activate this upgrade", creds, upgrade->config.name, cost);
			if (creds >= adata->minimumPartialPurchaseForAnnounce && creds > 0)
			{
				chat->SendAnyMessage(&pLL  , MSG_FUSCHIA, 0, NULL, "%s spent %ld credits on upgrade %s, %ld more credits are needed. Type ?buy %s to activate this upgrade.", p->name, creds, upgrade->config.name, cost, upgrade->config.name);
				chat->SendAnyMessage(&p2LL , MSG_ARENA  , 0, NULL, "%s spent %ld credits on upgrade %s, %ld more credits are needed. Type ?buy %s to activate this upgrade.", p->name, creds, upgrade->config.name, cost, upgrade->config.name);
			}
			return;
		}

		for (link = LLGetHead(&upgrade->partialPurchase.freqs); link; link = link->next)
			tFREE(link->data);

		LLEmpty(&upgrade->partialPurchase.freqs);

		adata->credits->AddCreditsPlayer(p, p->p_freq, -cost, false, false);
		chat->SendCmdMessage(p, "Purchased upgrade %s for %ld credits.", upgrade->config.name, cost);

		scope.type = T_FREQ;
		scope.u.freq.freq = p->p_freq;
		scope.u.freq.arena = p->arena;

		fdata = GetFreqData(arena, p->p_freq, true);
		LLAdd(&fdata->purchasedUpgrades, upgrade);

		chat->SendAnyMessage(&pLL , MSG_FUSCHIA, 0, NULL, "Upgrade %s purchased by %s.", upgrade->config.name, p->name);
		chat->SendAnyMessage(&p2LL, MSG_ARENA  , 0, NULL, "Upgrade %s purchased by %s.", upgrade->config.name, p->name);
		LLEmpty(&pLL);
		LLEmpty(&p2LL);
	}
	else if (upgrade->config.target == UPGRADETARGET_ARENA)
	{
		    pd->Lock();
		FOR_EACH_PLAYER(q)
		{
			if (q->arena == arena && q != p) // players that receive an arena message about the upgrade
			{
				if (q->type == T_CONT)
					LLAdd(&pLL, q);
				else
					LLAdd(&p2LL, q);
			}
		}
		pd->Unlock();

		cost -= upgrade->partialPurchase.arena_credits;

		if (cost > creds) // partial buy; player spends all his credits on the upgrade
		{
			upgrade->partialPurchase.arena_credits += creds;
			cost -= creds;
			adata->credits->AddCreditsPlayer(p, p->p_freq, -creds, false, false);

			chat->SendCmdMessage(p, "Spent %ld credits on upgrade %s. %ld more credits are needed to activate this upgrade", creds, upgrade->config.name, cost);
			if (creds >= adata->minimumPartialPurchaseForAnnounce)
			{
				chat->SendAnyMessage(&pLL, MSG_FUSCHIA, 0, NULL, "%s spent %ld credits on upgrade %s, %ld more credits are needed. Type ?buy %s to activate this upgrade.", p->name, creds, upgrade->config.name, cost, upgrade->config.name);
				chat->SendAnyMessage(&p2LL, MSG_ARENA, 0, NULL, "%s spent %ld credits on upgrade %s, %ld more credits are needed. Type ?buy %s to activate this upgrade.", p->name, creds, upgrade->config.name, cost, upgrade->config.name);
			}
			return;
		}

		upgrade->partialPurchase.arena_credits  = 0;

		adata->credits->AddCreditsPlayer(p, p->p_freq, -cost, false, false);
		chat->SendCmdMessage(p, "Purchased upgrade %s for %ld credits.", upgrade->config.name, cost);

		scope.type = T_ARENA;
		scope.u.arena = p->arena;

		struct arenaData *adata = GetArenaData(arena);
		LLAdd(&adata->purchasedUpgrades, upgrade);

		chat->SendAnyMessage(&pLL, MSG_FUSCHIA  , 0, NULL, "Upgrade %s purchased by %s.", upgrade->config.name, p->name);
		chat->SendAnyMessage(&p2LL, MSG_ARENA  , 0, NULL, "Upgrade %s purchased by %s.", upgrade->config.name, p->name);
		LLEmpty(&pLL);
		LLEmpty(&p2LL);
	}
	else
	{
		assertlm(false);
	}
	EnableUpgrade(&scope, upgrade, true);
}


static void SetUpgradeAttributes(Target *scope, struct upgradeData *upgrade)
{
	struct attributeData *attr;
	Link *l;

	assertlm(upgrade);
	assertlm(scope);

	if (upgrade->config.target == UPGRADETARGET_PLAYER)
	{
		assertlm(scope->type = T_PLAYER);
	}
	else if (upgrade->config.target == UPGRADETARGET_FREQ)
	{
		assertlm(scope->type = T_FREQ);
	}
	else if (upgrade->config.target == UPGRADETARGET_ARENA)
	{
		assertlm(scope->type = T_ARENA);
	}

	if (upgrade->setter)
	{
		for (l = LLGetHead(&upgrade->config.attributes); l; l = l->next)
		{
			attr = l->data;
			attrman->Lock();
			attrman->SetValue(scope, upgrade->setter, attr->property, attr->addition, attr->ship, attr->value);
			attrman->UnLock();
		}
	}
}

static void UnsetUpgradeAttributes(Target *scope, struct upgradeData *upgrade)
{
	struct attributeData *attr;
	Link *l;

	assertlm(upgrade);
	assertlm(scope);
	if (upgrade->config.target == UPGRADETARGET_PLAYER)
	{
		assertlm(scope->type = T_PLAYER);
	}
	else if (upgrade->config.target == UPGRADETARGET_FREQ)
	{
		assertlm(scope->type = T_FREQ);
	}
	else if (upgrade->config.target == UPGRADETARGET_ARENA)
	{
		assertlm(scope->type = T_ARENA);
	}

	if (upgrade->setter)
	{
		for (l = LLGetHead(&upgrade->config.attributes); l; l = l->next)
		{
			attr = l->data;
			attrman->Lock();
			attrman->UnsetValue(scope, upgrade->setter, attr->property, attr->ship);
			attrman->UnLock();
		}
	}
}

static void EnableUpgrade(Target *scope, struct upgradeData *upgrade, bool doActions) // Activate the upgrade, this usually happends after a purchase
{
	Link *l;
	struct actionData *action;

	 assertlm(upgrade);
	assertlm(scope);
	if (upgrade->config.target == UPGRADETARGET_PLAYER)
	{
		assertlm(scope->type = T_PLAYER);
	}
	else if (upgrade->config.target == UPGRADETARGET_FREQ)
	{
		assertlm(scope->type = T_FREQ);
	}
	else if (upgrade->config.target == UPGRADETARGET_ARENA)
	{
		assertlm(scope->type = T_ARENA);
	}

	SetUpgradeAttributes(scope, upgrade);

	if (doActions)
	{
		for (l = LLGetHead(&upgrade->config.actions); l; l = l->next)
		{
			action = l->data;
			DoItemAction(NULL, scope, action);
		}
	}
}

static int RemoveUpgradeOnPlayer_HashEnumPlayerData(const char *key, void *val, void *clos)
{
	struct playerData *pdata = (struct playerData *) val;
	if (!pdata->p) return 1; // player is not in the arena
	Target target;
	target.type = T_PLAYER;
	target.u.p = pdata->p;
	RemoveUpgrade(&target, (struct upgradeData *) clos);

	return 1;
}

static void RemoveUpgradeEverywhere(Arena *arena, struct upgradeData *upgrade)
{
	Target target;
	Link *link;
	struct arenaData *adata = GetArenaData(arena);

	// Remove this upgrade everywhere
	assertlm(arena);
	assertlm(upgrade);

	if (upgrade->setter)
	{
		attrman->Lock();
		attrman->UnregisterSetter(upgrade->setter);
		attrman->UnLock();
		upgrade->setter = 0;
	}

	switch (upgrade->config.target)
	{
		case UPGRADETARGET_PLAYER:
			target.type = T_PLAYER;

			HashEnum(adata->playerData, RemoveUpgradeOnPlayer_HashEnumPlayerData, upgrade);

			break;
		case UPGRADETARGET_FREQ:
			target.type = T_FREQ;
			target.u.freq.arena = arena;
			struct freqData *fdata;

			for (link = LLGetHead(&adata->freqs); link; link = link->next)
			{
				fdata = link->data;
				target.u.freq.freq = fdata->freq;
				RemoveUpgrade(&target, upgrade);
			}

			break;
		case UPGRADETARGET_ARENA:
			target.type = T_ARENA;
			target.u.arena = arena;

			RemoveUpgrade(&target, upgrade);
			break;
		default:
			assertlm(false);
			return;
	}
}

static void RemoveUpgrade(Target *target, struct upgradeData *upgrade)
{
	struct playerData *pdata;
	struct freqData *fdata;
	struct arenaData *adata;


	// Remove the upgrade from the specified target
	assertlm(upgrade);

	switch (target->type)
	{
		// Unset this upgrade as purchased
		case T_PLAYER:
			assertlm(upgrade->config.target == UPGRADETARGET_PLAYER);

			pdata = GetPlayerData(target->u.p);
			LLRemoveAll(&pdata->purchasedUpgrades, upgrade);
			break;
		case T_FREQ:
			assertlm(upgrade->config.target == UPGRADETARGET_FREQ);

			fdata = GetFreqData(target->u.freq.arena, target->u.freq.freq, false);
			if (fdata)
				LLRemoveAll(&fdata->purchasedUpgrades, upgrade);
			break;
		case T_ARENA:
			assertlm(upgrade->config.target == UPGRADETARGET_ARENA);

			adata = GetArenaData(target->u.arena);
			LLRemoveAll(&adata->purchasedUpgrades, upgrade);
			break;
		default:
			assertlm(false);
			return;
	}

	UnsetUpgradeAttributes(target, upgrade);
}

static helptext_t Cbuy_help =
	"Targets: none\n"
	"Args: <item>\n"
	"Buys the specified item, if you have enough credits\n"
	;
static void Cbuy(const char *tc, const char *params, Player *p, const Target *target)
{
	Arena *arena = p->arena;
	struct freqData *fdata;
	struct arenaData *adata = GetArenaData(arena);
	struct buyItemData *buyitem;
	bool hasUpgrade;
	unsigned int categories, items;
	LinkedList buyItems = LL_INITIALIZER;
	Link *l;
	enum REQUIREMENTTYPE failedRequirement;
	int a;
	bool onFreq, boughtSomething;
	char buf [BUF_CHARS+1];


	if (!arena) return;
	if (!adata->initialized) return;
	fdata = GetFreqData(arena, p->p_freq, false);
	char *s;

	if (!adata->running)
	{
		chat->SendCmdMessage(p, "You can not buy items when the game is not running.");
		return;
	}

	if (!fdata)
	{
		chat->SendCmdMessage(p, "Your freq can not buy anything.");
		return;
	}

	if (p->p_ship == SHIP_SPEC)
	{
		chat->SendCmdMessage(p, "Your can not purchase anything in spectator mode");
		return;
	}

	strncpy(buf, params, sizeof(buf)); //this may change later to allow for more arguments
	buf[sizeof(buf) - 1] = 0;
	ToLowerStr(buf);
	s = strchr(buf, 0) - 1;
	while (s >= buf && *s == ' ')
	{
		*s = 0;
		s--;
	}
	s = buf;
	while (*s == ' ')
	{
		s++;
	}

	if (!*s) //list categories
	{
		categories = ListCategories(p, fdata, NULL);
		items = ListItems(p, fdata, NULL, !categories);

		if (!categories && !items)
			chat->SendCmdMessage(p, "There are no items to display.");

		return;
	}

	HashGetAppend(adata->buyList, s, &buyItems);

	boughtSomething = false;

	for (l = LLGetHead(&buyItems); l; l = l->next)
	{
		buyitem = l->data;
		onFreq = false;
		hasUpgrade = false;

		#define tPARSEFREQS(STRUCT) \
		for (a = 0; a < STRUCT->config.freq_length; a++) \
		{ \
			if (STRUCT->config.freqs[a] == p->p_freq) \
			{ \
				onFreq = true; \
				break; \
			} \
		}

		if (buyitem->type == BUYITEM_CATEGORY) //list all items of this category
		{
			tPARSEFREQS(buyitem->u.category)

			if (!onFreq)
				continue;

			RequirementsMetCategory(p, buyitem->u.category, &failedRequirement, false);
			if (failedRequirementHides(failedRequirement))
				continue;

			categories = ListCategories(p, fdata, buyitem->u.category);
			items = ListItems(p, fdata, buyitem->u.category, !categories);

			if (!categories && !items)
				chat->SendCmdMessage(p, "There are no items to display.");

			boughtSomething = true;
			break;
		}
		else if (buyitem->type == BUYITEM_UPGRADE) //buy an upgrade
		{
			tPARSEFREQS(buyitem->u.upgrade)

			if (!onFreq)
				continue;

			if (HasUpgrade(p, buyitem->u.upgrade))
			{
				hasUpgrade = true;
				continue; //keep looking, if nothing is found after this, `hasUpgrade` will cause an error message
			}

			RequirementsMetUpgrade(p, buyitem->u.upgrade, &failedRequirement, false);
			if (failedRequirementHides(failedRequirement))
				continue;

			BuyUpgrade(p, buyitem->u.upgrade);
			boughtSomething = true;
			break;
		}
		else if (buyitem->type == BUYITEM_ITEM) //buy an item
		{
			tPARSEFREQS(buyitem->u.item)

			if (!onFreq)
				continue;

			RequirementsMetItem(p, buyitem->u.item, &failedRequirement, false);
			if (failedRequirementHides(failedRequirement))
				continue; // failed requirement hides the item, so pretend the item does not exist at all (may create multiple items with the same name this way)

			BuyItem(p, buyitem->u.item);
			boughtSomething = true;
			break;
		}

		#undef tPARSEFREQS
	}

	if (LLIsEmpty(&buyItems))
	{
		chat->SendCmdMessage(p, "There is no item %s", s);
		return;
	}
	else if (!boughtSomething)
	{
		if (hasUpgrade)
		{
			chat->SendCmdMessage(p, "You already have the upgrade %s", s);
		}
		else
		{
			chat->SendCmdMessage(p, "Item %s is not available", s);
		}
		return;
	}

	LLEmpty(&buyItems);
}

static bool HasUpgrade(Player *p, struct upgradeData *upgrade)
{
	struct playerData *pdata;
	struct freqData *fdata;
	struct arenaData *adata;

	assertlm(p->arena);

	switch (upgrade->config.target)
	{
		case UPGRADETARGET_PLAYER:
			pdata = GetPlayerData(p);
			if (LLMember(&pdata->purchasedUpgrades, upgrade))
				return true;
			break;
		case UPGRADETARGET_FREQ:
			fdata = GetFreqData(p->arena, p->p_freq, true);
			if (LLMember(&fdata->purchasedUpgrades, upgrade))
				return true;
			break;
		case UPGRADETARGET_ARENA:
			adata = GetArenaData(p->arena);
			if (LLMember(&adata->purchasedUpgrades, upgrade))
				return true;
			break;
		default:
			assertlm(false);
			return false;
	}
	return false;
}

// Has a specific requirment been met?
static bool RequirementMet(Player *p, struct requirementData *requirement, const char *showMessage)
{
#ifdef USE_STRUCTURES
	Istructures *structures;
#endif
#ifdef USE_SUPERWEAPON
	Isuperweapon *superweapon;
#endif
	int freqCharge;
	int playerCharge;
	switch (requirement->requirementType)
	{
		case REQUIREMENT_UPGRADE:
			if (HasUpgrade(p, requirement->value.upgrade.upgrade))
				return true;

			if (showMessage)
				chat->SendCmdMessage(p, "To buy item %s, you need the upgrade %s", showMessage,  requirement->value.upgrade.upgrade->config.name);
			break;
#ifdef USE_STRUCTURES
		case REQUIREMENT_STRUCTURETYPE:

			structures = mm->GetInterface(I_STRUCTURES, p->arena);
			if (structures &&  p->p_freq >= 0 && p->p_freq < STRUCTURES_FREQS)
			{
				structureTypeData *stdata = structures->GetStructureTypeData(p->arena, p->p_freq, requirement->value.structuretype.type);
				if (stdata->structureCount > 0)
					return true;

				if (showMessage)
					chat->SendCmdMessage(p, "To buy item %s, you need to have a %s building", showMessage, structureTypeNames[requirement->value.structuretype.type]);
			}
			mm->ReleaseInterface(structures);

			break;
		case REQUIREMENT_STRUCTURE:
			structures = mm->GetInterface(I_STRUCTURES, p->arena);
			if (structures)
			{
				structureData *sdata = structures->GetStructureData(p->arena, requirement->value.structure.structure);
				if (sdata && sdata->alive)
					return true;

				if (showMessage)
					chat->SendCmdMessage(p, "You do not have the proper requirements to buy item %s.", showMessage);
			}
			mm->ReleaseInterface(structures);
			break;
#endif
		case REQUIREMENT_SHIP:
			if (requirement->value.ship.ship & (1 << p->p_ship))
				return true;

			if (showMessage)
				chat->SendCmdMessage(p, "You can not buy item %s in your current ship.", showMessage);
			break;
		case REQUIREMENT_SAFEZONE:
			if (p->position.status & STATUS_SAFEZONE)
				return true;

			if (showMessage)
				chat->SendCmdMessage(p, "You need to be in a safe zone to buy item %s.", showMessage);
			break;
		case REQUIREMENT_REGION:
			if (map->Contains(requirement->value.region.region, p->position.x >> 4, p->position.y >> 4))
				return true;

			if (showMessage)
				chat->SendCmdMessage(p, "You can not buy item %s from your current location.", showMessage);
			break;
#ifdef USE_SUPERWEAPON
		case REQUIREMENT_SUPERWEAPONCHARGE:
			superweapon = mm->GetInterface(I_SUPERWEAPON, p->arena);
			if (superweapon)
			{
				freqCharge = superweapon->GetChargeFreq(p->arena, p->p_freq, requirement->value.superweaponcharge.superweapon);
				playerCharge = superweapon->GetChargePlayer(p, requirement->value.superweaponcharge.superweapon);
				freqCharge -= requirement->value.superweaponcharge.freqCharge;
				playerCharge -= requirement->value.superweaponcharge.playerCharge;

				if ( freqCharge >= 0 && playerCharge >= 0)
					return true;

				freqCharge *= -1;
				playerCharge *= -1;

				if (showMessage)
					chat->SendCmdMessage(p, "The %s has not finnished charging yet, please wait %d seconds", showMessage, (freqCharge > playerCharge ? freqCharge : playerCharge) / 100);

			}
			mm->ReleaseInterface(superweapon);
			break;
#endif
		default:
			assertlm(false);
			break;
	}
	return false;
}

// Are all the requirements met for this category?
static bool RequirementsMetCategory(Player *p, struct categoryData *category, enum REQUIREMENTTYPE *failedRequirement, bool showMessage)
{
	Link *l;
	struct requirementData *requirement;
	assertlm(p->arena);

	if (failedRequirement)
		*failedRequirement = REQUIREMENT_UNSET;

	for (l = LLGetHead(&category->config.requirements); l; l = l->next)
	{
		requirement = l->data;
		if (!RequirementMet(p, requirement, showMessage ? category->config.name : NULL))
		{
			if (failedRequirement)
				*failedRequirement = requirement->requirementType;

			return false;
		}
	}

	return true;
}

static bool RequirementsMetUpgrade(Player *p, struct upgradeData *upgrade, enum REQUIREMENTTYPE *failedRequirement, bool showMessage)
{
	Link *l;
	struct requirementData *requirement;
	assertlm(p->arena);

	if (failedRequirement)
		*failedRequirement = REQUIREMENT_UNSET;

	for (l = LLGetHead(&upgrade->config.requirements); l; l = l->next)
	{
		requirement = l->data;
		if (!RequirementMet(p, requirement, showMessage ? upgrade->config.name : NULL))
		{
			if (failedRequirement)
				*failedRequirement = requirement->requirementType;

			return false;
		}
	}
	return true;
}

static bool RequirementsMetItem(Player *p, struct itemData *item, enum REQUIREMENTTYPE *failedRequirement, bool showMessage)
{
	Link *l;
	struct requirementData *requirement;
	assertlm(p->arena);

	if (failedRequirement)
		*failedRequirement = REQUIREMENT_UNSET;

	for (l = LLGetHead(&item->config.requirements); l; l = l->next)
	{
		requirement = l->data;
		if (!RequirementMet(p, requirement, showMessage ? item->config.name : NULL))
		{
			if (failedRequirement)
				*failedRequirement = requirement->requirementType;

			return false;
		}
	}

	return true;
}

EXPORT const char info_advbuy[] = "Advanced Buy by JoWie ("BUILDDATE")\n";
static void ReleaseInterfaces()
{
	mm->ReleaseInterface(cfg       );
	mm->ReleaseInterface(cmd       );
	mm->ReleaseInterface(capman    );
	mm->ReleaseInterface(chat      );
	mm->ReleaseInterface(pd	);
	mm->ReleaseInterface(aman      );
	mm->ReleaseInterface(attrman   );
	mm->ReleaseInterface(capman    );
	mm->ReleaseInterface(lm	);
	mm->ReleaseInterface(game      );
	mm->ReleaseInterface(map       );
}

EXPORT int MM_advbuy(int action, Imodman *mm_, Arena *arena)
{
	if (action == MM_LOAD)
	{
		mm = mm_;

		chat	    = mm->GetInterface(I_CHAT      , ALLARENAS);
		capman	  = mm->GetInterface(I_CAPMAN    , ALLARENAS);
		cfg	     = mm->GetInterface(I_CONFIG    , ALLARENAS);
		cmd	     = mm->GetInterface(I_CMDMAN    , ALLARENAS);
		pd	      = mm->GetInterface(I_PLAYERDATA, ALLARENAS);
		aman	    = mm->GetInterface(I_ARENAMAN  , ALLARENAS);
		attrman	 = mm->GetInterface(I_ATTRMAN   , ALLARENAS);
		capman	  = mm->GetInterface(I_CAPMAN    , ALLARENAS);
		lm	      = mm->GetInterface(I_LOGMAN    , ALLARENAS);
		game	    = mm->GetInterface(I_GAME      , ALLARENAS);
		map		= mm->GetInterface(I_MAPDATA   , ALLARENAS);

		if (!chat || !capman || !cfg || !cmd || !pd || !aman || !attrman || !capman || !lm || !game || !map)
		{
			printf("<advbuy> Missing Interface\n");

			ReleaseInterfaces();

			return MM_FAIL;
		}

		arenaKey = aman->AllocateArenaData(sizeof(struct arenaData));

		if (arenaKey == -1)
		{
			ReleaseInterfaces();

			return MM_FAIL;
		}


		playerKey = pd->AllocatePlayerData(sizeof(struct playerData_pd));

		if (playerKey == -1)
		{
			if (arenaKey != -1) aman->FreeArenaData(arenaKey);

			ReleaseInterfaces();

			return MM_FAIL;
		}

		usedAttributes = HashAlloc();

		return MM_OK;
	}
	else if (action == MM_UNLOAD)
	{
		if (advbuy.head.arena_registrations)
			return MM_FAIL;

		cmd->RemoveCommand("buy", Cbuy, ALLARENAS);

		mm->UnregCallback(CB_ARENAACTION, ArenaAction, ALLARENAS);

		aman->FreeArenaData(arenaKey);
		pd->FreePlayerData(playerKey);

		ReleaseInterfaces();

		HashFree(usedAttributes);
		usedAttributes = NULL;

		return MM_OK;
	}
	else if (action == MM_ATTACH)
	{
		struct arenaData *adata = GetArenaData(arena);
		adata->credits = mm->GetInterface(I_GAMECREDITS, arena);
		if (!adata->credits)
		{
			lm->LogA(L_ERROR, "advbuy", arena, "Unable to attach, missing interface (wrong attach order?)");
			return MM_FAIL;
		}

		mm->RegInterface(&advbuy, arena);

		mm->RegCallback(CB_ARENAACTION, ArenaAction, arena);
		mm->RegCallback(CB_PLAYERACTION, PlayerActionCB, arena);
		cmd->AddCommand("buy", Cbuy, arena, Cbuy_help);



		if (!adata->initialized)
			InitializeArena(arena);

		return MM_OK;
	}
	else if (action == MM_DETACH)
	{
		if (mm->UnregInterface(&advbuy, arena))
			return MM_FAIL;

		struct arenaData *adata = GetArenaData(arena);
		mm->ReleaseInterface(adata->credits);

		cmd->RemoveCommand("buy", Cbuy, arena);
		mm->UnregCallback(CB_ARENAACTION, ArenaAction, arena);
		mm->UnregCallback(CB_PLAYERACTION, PlayerActionCB, arena);


		if (adata->initialized) CleanupArena(arena);
		return MM_OK;
	}

	return MM_FAIL;
}

#undef tFREE
