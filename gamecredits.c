/*
Copyright (c) 2009 Joris v/d Wel

This file is part of ASSS Advanced Buy

   ASSS Advanced Buy is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, version 3 of the License.

   ASSS Advanced Buy is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with ASSS Advanced Buy.  If not, see <http://www.gnu.org/licenses/>.

*/
/**
Credits

Credits are per game, per player, per freq

Credits reset when:
	- game is not running
	- game end
	- game start

[GameCredits]
Enabled = 0

OtherFreqGainPermille = 200

KillCreditsEnabled = 0
KillAnnounce = 1

KillExtraCredits = 100
KillMaximumCredits = 10000
KillerSpreeCredits = 25
KilledSpreeCredits = 100
KillCreditsShareBetweenFreqs = 1
KillerTeammateCreditsPromille = 100

;Distance falloff divisor in pixels^2
KillerTeammateDistance = 300000

GoalCreditsEnabled = 0
GoalExtraCredits = 500
GoalCreditsPerPlaying = 100
GoalMaximumCredits = 10000
GoalSplit = 1
GoalScorerBonusPromille = 250

OreCreditsEnabled = 0
OreExtraCredits = 500
OreCreditsPerPlaying = 100
OreMaximumCredits = 10000
OreSplit = 1
OreScorerBonusPromille = 250

LVZEnabled = 1
LVZDigits = 6
LVZObjectStart = 1000
LVZImageStart = 0
*/

#include <time.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "asss.h"

#include "gamecredits.h"
#include "attrman.h"

#define FREQCOUNT 3

//10 digits is about the maximum size of an 32 bit integer (2^31 = 2 147 483 648)
#define LVZ_MAXDIGITS 10

//How often to update
#define LVZ_UPDATEDELAY 100

//How often to check if an update is needed
#define LVZ_UPDATETIMER 10

#ifdef NDEBUG
#define assertlm(x) ((void)0)
#else
#define assertlm(e) if (!(e)) AssertLM(#e, __FILE__, __FUNCTION__, __LINE__)
#endif

static struct arenaData* GetArenaData(Arena *arena);
static struct playerData* GetPlayerData(Player *p);
static void AddCreditsPlayer(Player *p, int freq, long credits, bool otherFreqGain, bool overflowCreditsIntoTeamMembers);
static void AddCreditsFreq(Arena *arena, int freq, long credits, bool otherFreqGain, bool overflowCreditsIntoTeamMembers);
static long SetCredits(Player *p, int freq, long credits);
static long GetCredits(Player *p, int freq);
static void ResetCredits(Player *p);
static bool StartGame (Arena *arena);
static void StopGame (Arena *arena);
static void PlayerAction (Player *p, int action, Arena *arena);
static void ShipFreqChangeCB(Player *p, int newship, int oldship, int newfreq, int oldfreq);
static void Kill(Arena *arena, Player *killer, Player *killed, int bounty, int flags, int pts, int green);
static bool  Goal (Arena *arena, Player *p, int freq, int bid, int x, int y);
static void MineOre(Arena *arena, Player *miner, int freq, int ore);
static void ReadSettings (Arena *arena);

static void UpdateLVZ(Player *p);
static int  UpdateLVZ_timer(void *nothing);

static void Ccredits(const char *tc, const char *params, Player *p, const Target *target);
static void Cgrant(const char *tc, const char *params, Player *p, const Target *target);

// Interfaces
static Imodman            *mm;
static Imainloop          *ml;
static Ichat              *chat;
static Iconfig            *cfg;
static Icmdman            *cmd;
static Iconfig            *cfg;
static Iplayerdata        *pd;
static Iarenaman          *aman;
static Iprng              *prng;
static Ilogman            *lm;
static Iobjects           *objs;
static Iattrman           *attrman;
static Igamecredits       credits =
{
	INTERFACE_HEAD_INIT(I_GAMECREDITS, "gamecredits")

	AddCreditsPlayer,
	AddCreditsFreq,
	SetCredits,
	GetCredits,
	ResetCredits,
	StartGame,
	StopGame,
	Goal,
	MineOre
};

static int arenaKey = -1;
static int playerKey = -1;
static AttrmanSetter settingSetter;

struct arenaData
{
	bool running;

	bool enabled;

	bool killCreditsEnabled;
	bool killAnnounce;
	int killExtraCredits;
	int killMaximumCredits;
	int killerSpreeCredits;
	int killedSpreeCredits;
	bool killCreditsShareBetweenFreqs;
	double killerTeammateCreditsMultiplier;
	int killerTeammateDistance;

	bool goalCreditsEnabled;
	int goalExtraCredits;
	int goalCreditsPerPlaying;
	int goalMaximumCredits;
	bool goalSplit;
	double goalScorerBonusMultiplier;

	bool oreCreditsEnabled;
	int oreCalculatePer;
	int oreExtraCredits;
	int oreCreditsPerPlaying;
	int oreMaximumCredits;
	bool oreSplit;
	double oreScorerBonusMultiplier;

	double otherFreqGainMultiplier;

	bool LVZEnabled;
	int LVZDigits;
	int LVZObjectStart;
	int LVZImageStart;

	HashTable *playerCredits; // struct creditData*
};

struct creditData
{
	long credits[FREQCOUNT]; //the credits per freq (Freq changing is allowed)
};

struct playerData
{
	Arena *arena;
	bool LVZVisible; //Is the LVZ Currently visible?
	int LVZFreq; //the freq the LVZ belong to
	int LVZOldCredits; //the credits the LVZ are currently displaying
	ticks_t LVZLastUpdate;
	int killsWithoutDeath;

	struct creditData cdata;
};


static void AssertLM (const char* e, const char* file, const char *function, int line)
{
	if (lm) lm->Log(L_ERROR | L_SYNC, "<gamecredits> Assertion \"%s\" failed: file \"%s\", line %d, function %s()\n", e, file, line, function);
	fullsleep(500);
	Error(EXIT_GENERAL, "\nAssertion \"%s\" failed: file \"%s\", line %d, function %s()\n", e, file, line, function);
}

static struct arenaData* GetArenaData(Arena *arena)
{
	assertlm(arena);
	struct arenaData *adata = P_ARENA_DATA(arena, arenaKey);
	assertlm(adata);
	if (!adata->playerCredits)
	{
		adata->playerCredits = HashAlloc();
	}
	return adata;
}

static struct playerData* GetPlayerData(Player *p)
{
	assertlm(p);
	struct playerData *pdata = PPDATA(p, playerKey);
	assertlm(pdata);

	if (p->arena && p->arena != pdata->arena)
	{
		pdata->arena = p->arena;
		pdata->LVZVisible = false;
		pdata->LVZFreq = -1;
		pdata->LVZOldCredits = 0;
		pdata->LVZLastUpdate = 0;
	}
	return pdata;
}

static struct creditData* GetCreditData(Arena *arena, Player *p)
{
	assertlm(p);
	assertlm(arena);
	struct arenaData *adata = GetArenaData(arena);

	struct creditData *cdata = HashGetOne(adata->playerCredits, p->name);
	if (!cdata)
	{

		cdata = amalloc(sizeof(struct creditData));
		memset(cdata, 0, sizeof(struct creditData));
		HashAdd(adata->playerCredits, p->name, cdata);

	}
	return cdata;
}

static void AddCreditsPlayer(Player *p, int freq, long credits, bool otherFreqGain, bool overflowCreditsIntoTeamMembers)
{
	assertlm(p && p->arena);

	struct arenaData *adata = GetArenaData(p->arena);

	if (!adata->enabled) return;
	if (freq < 0) freq = p->p_freq;
	if (freq < 0 || freq >= FREQCOUNT) return;

	int a;
	long creditsWrongFreq;
	long maxCreditsWrongFreq;

	long oldCredits;
	long diffCredits;
	long newCredits;
	long maxCredits;

	Link *link;
	Player *p2;
	int playersOnFreq;

	attrman->Lock();
	maxCredits = attrman->GetPlayerValue(p, "gamecredits::maxcredits", NULL);
	attrman->UnLock();

	if (otherFreqGain)
	{
		creditsWrongFreq = credits * adata->otherFreqGainMultiplier;
		maxCreditsWrongFreq = maxCredits * adata->otherFreqGainMultiplier; //would be better to use the maxCredits of the other freq, but it doesn't really matter that much
		for (a = 0; a < FREQCOUNT; a++)
		{
			if (a != freq)
			{
				//wrong freq
				oldCredits = GetCredits(p, a);
				newCredits = oldCredits + creditsWrongFreq;

				if (creditsWrongFreq >= 0)
				{
					if (oldCredits < maxCreditsWrongFreq) // if the old credits was over the max, do nothing
					{
						if (newCredits > maxCreditsWrongFreq)
							newCredits = maxCreditsWrongFreq;

						SetCredits(p, a, newCredits);
					}
				}
				else // taking away credits
				{
					if (newCredits < 0) newCredits = 0;
					SetCredits(p, a, newCredits);
				}
			}
		}
	}

	oldCredits = GetCredits(p, freq);
	newCredits = oldCredits + credits;

	if (credits >= 0)
	{
		if (newCredits > maxCredits) //going over the credit max, split the remaining credits among the rest of the team
		{
			diffCredits = newCredits - maxCredits;
			newCredits = maxCredits;

			if (overflowCreditsIntoTeamMembers)
			{
				playersOnFreq = 0;
                                
				pd->Lock();
				FOR_EACH_PLAYER(p2)
				{
					if (p2->type != T_FAKE && p->arena == p2->arena && p->p_freq == p2->p_freq)
					{
						//correct arena and freq
						playersOnFreq++;
					}
				}
				pd->Unlock();

				if (playersOnFreq > 0)
				{
					AddCreditsFreq(p->arena, freq, diffCredits / playersOnFreq, false, false);
				}
			}
		}

		if (oldCredits < maxCredits) // do not set the credits if the existing credits are over the maximum (this happends when the maximum is suddenly lowered)
			SetCredits(p, freq, newCredits);
	}
	else
	{
		if (newCredits < 0) newCredits = 0;
		SetCredits(p, freq, newCredits);
	}
}

static void AddCreditsFreq(Arena *arena, int freq, long credits, bool otherFreqGain, bool overflowCreditsIntoTeamMembers)
{
	assertlm (arena);
	if (freq < 0 || freq >= FREQCOUNT) return;
	struct arenaData *adata = GetArenaData(arena);
	if (!adata->enabled) return;
	Player *p;
	Link *link;

	long creditsWrongFreq = credits * adata->otherFreqGainMultiplier;
	long maxCredits;
	long newCredits;
	long diffCredits;
	long oldCredits;
	int playersOnFreq;

	playersOnFreq = 0;
	attrman->Lock();
	pd->Lock();

	if (overflowCreditsIntoTeamMembers)
	{
		FOR_EACH_PLAYER(p)
		{
			if (p->type != T_FAKE && arena == p->arena && p->p_freq == freq)
			{
				//correct arena and freq
				playersOnFreq++;
			}
		}
	}

	FOR_EACH_PLAYER(p)
	{
		if (p->type != T_FAKE && arena == p->arena && freq >= 0 && freq < FREQCOUNT)
		{
			//correct arena and valid freq
			if (freq == p->p_freq) //correct freq
			{
				maxCredits = attrman->GetPlayerValue(p, "gamecredits::maxcredits", NULL);
				oldCredits = GetCredits(p, freq);
				newCredits = oldCredits + credits;

				if (credits >= 0)
				{
					if (newCredits > maxCredits)
					{
						diffCredits = newCredits - oldCredits;
						newCredits = maxCredits;

						if (overflowCreditsIntoTeamMembers && playersOnFreq > 0)
						{
							AddCreditsFreq(arena, freq, diffCredits / playersOnFreq, false, false);
						}
					}

					if (oldCredits < maxCredits)
						SetCredits(p, freq, newCredits);
				}
				else
				{
					if (newCredits < 0) newCredits = 0;
					SetCredits(p, freq, newCredits);
				}
			}
			else if (otherFreqGain) //wrong freq; give the player reduced credits, BUT not for the freq he is currently on
			{
				maxCredits = attrman->GetPlayerValue(p, "gamecredits::maxcredits", NULL) * adata->otherFreqGainMultiplier; //would be batter to use the maxCredits of the other freq, but it doesn't really matter that much
				oldCredits =  GetCredits(p, freq);
				newCredits = oldCredits + creditsWrongFreq;

				if (creditsWrongFreq >= 0)
				{
					if (oldCredits < maxCredits)
					{
						if (newCredits > maxCredits) newCredits = maxCredits;
						SetCredits(p, freq, newCredits);
					}
				}
				else
				{
					if (newCredits < 0) newCredits = 0;
					SetCredits(p, freq, newCredits);
				}
			}
		}
	}
	pd->Unlock();
	attrman->UnLock();
}

static long SetCredits(Player *p, int freq, long credits)
{
	assertlm(p && p->arena);
	if (freq < 0) freq = p->p_freq;
	if (freq < 0 || freq >= FREQCOUNT) return 0;
	if (p->type == T_FAKE) return 0;
	struct arenaData *adata = GetArenaData(p->arena);
	if (!adata->enabled) return 0;

	struct playerData *pdata = GetPlayerData(p);

	if (!adata->running) return 0;


	pdata->cdata.credits[freq] = credits;


	return pdata->cdata.credits[freq];

}

static long GetCredits(Player *p, int freq)
{
	assertlm(p && p->arena);
	if (freq < 0) freq = p->p_freq;
	if (freq < 0 || freq >= FREQCOUNT) return 0;
	if (p->type == T_FAKE) return 0;
	struct arenaData *adata = GetArenaData(p->arena);
	if (!adata->enabled) return 0;

	struct playerData *pdata = GetPlayerData(p);

	if (!adata->running) return 0;

	return pdata->cdata.credits[freq];

}

static void ResetCredits(Player *p)
{
	assertlm(p && p->arena);
	if (p->type == T_FAKE) return;

	struct arenaData *adata = GetArenaData(p->arena);
	if (!adata->enabled) return;
	struct playerData *pdata = GetPlayerData(p);

	int a;


	for (a = 0; a < FREQCOUNT; a++)
	{
		pdata->cdata.credits[a] = 0;
	}

}

static void UpdateLVZ(Player *p)
{
	assertlm (objs);
	assertlm (p && p->arena);

	struct arenaData *adata = GetArenaData(p->arena);
	assertlm (adata->LVZEnabled); //after this, safe to assume LVZDigits, LVZObjectStart, LVZImageStart are valid

	struct playerData *pdata = GetPlayerData(p);

	int a;

	int freq = p->p_freq;

	Target tgt;
	tgt.type = T_PLAYER;
	tgt.u.p = p;

	short id[LVZ_MAXDIGITS];
	char ons[LVZ_MAXDIGITS];

	//The player does not have credits in the current state
	if (freq < 0 || freq >= FREQCOUNT || !adata->running)
	{
		//hide the lvz
		if (pdata->LVZVisible)
		{
			for (a = 0; a < adata->LVZDigits; a++)
			{
				id[a] = adata->LVZObjectStart + a;
				ons[a] = 0;
			}

			objs->ToggleSet(&tgt, id, ons, adata->LVZDigits);

			pdata->LVZVisible = false;

		}
		return;
	}

	long credits = GetCredits(p, freq);
	if (credits < 0) credits = 0;

	int digit, oldDigit;
	long value = credits, oldValue = pdata->LVZOldCredits;

	//nothing has changed, no need to update
	if (pdata->LVZFreq == freq && pdata->LVZVisible && pdata->LVZOldCredits == credits)
		return;


	if (adata->LVZDigits > LVZ_MAXDIGITS)
		adata->LVZDigits = LVZ_MAXDIGITS;


	for (a = 0; a < adata->LVZDigits; a++)
	{
		id[a] = adata->LVZObjectStart + a;

		if (value == 0 && a != 0) //no leading zero's
		{
			ons[a] = 0;
		}
		else
		{
			digit = value % 10;
			value /= 10;

			oldDigit = oldValue % 10;
			oldValue /= 10;

			ons[a] = 1;

			if (digit != oldDigit || !pdata->LVZVisible)
				objs->Image(&tgt, id[a], adata->LVZImageStart + digit);
		}
	}

	objs->ToggleSet(&tgt, id, ons, adata->LVZDigits);

	pdata->LVZFreq = freq;
	pdata->LVZVisible = true;
	pdata->LVZOldCredits = credits;

}

static int UpdateLVZ_timer(void *nothing)
{
	ticks_t now = current_ticks();
	struct playerData *pdata;
	struct arenaData *adata;

	Link *link;
	Player *p;

	assertlm(objs);

	pd->Lock();
	FOR_EACH_PLAYER(p)
	{
		if (p && p->arena && p->type != T_FAKE)
		{
			adata = GetArenaData(p->arena);
			pdata = GetPlayerData(p);

			//credits is running in this arena, lvz is enabled, update is needed
			if (adata && pdata && adata->enabled && adata->LVZEnabled && pdata->LVZLastUpdate + LVZ_UPDATEDELAY < now)
			{
				UpdateLVZ(p);

				pdata->LVZLastUpdate = now;

			}
		}
	}

	pd->Unlock();

	return 1;
}

static void ReadSettings(Arena *arena)
{
	assertlm (arena);

	Target tgtArena;
	tgtArena.type = T_ARENA;
	tgtArena.u.arena = arena;
	struct arenaData *adata = GetArenaData(arena);
	ConfigHandle ch = arena->cfg;


	adata->enabled = true;
	attrman->Lock();

	/* cfghelp: GameCredits:OtherFreqGainPermille, arena, int, def: 200
	* How many credits (in permille) you get for freqs you are not on */
	adata->otherFreqGainMultiplier = cfg->GetInt(ch, "GameCredits", "OtherFreqGainPermille", 200) / 1000.0;

	/* cfghelp: GameCredits:KillCreditsEnabled, arena, int, def: 0, range:0-1
	* Do players recieve credits for kills */
	adata->killCreditsEnabled = cfg->GetInt(ch, "GameCredits", "KillCreditsEnabled", false);
	
	/* cfghelp: GameCredits:KillAnnounce, arena, int, def: 0, range:0-1
	* Send arena messages for kills */
	adata->killAnnounce = cfg->GetInt(ch, "GameCredits", "KillAnnounce", false);

	/* cfghelp: GameCredits:KillExtraCredits, arena, int, def: 100
	* Credits added to every kill */
	adata->killExtraCredits = cfg->GetInt(ch, "GameCredits", "KillExtraCredits", 100);
	
	/* cfghelp: GameCredits:KillMaximumCredits, arena, int, def: 20000
	* Maximum Credits you can receive for a kill */
	adata->killMaximumCredits = cfg->GetInt(ch, "GameCredits", "KillMaximumCredits", 20000);
	
	/* cfghelp: GameCredits:KillerSpreeCredits, arena, int, def: 0
	* When the killer is on a spree, he receives this much credits extra for every kill */
	adata->killerSpreeCredits = cfg->GetInt(ch, "GameCredits", "KillerSpreeCredits", 0);
	
	/* cfghelp: GameCredits:KillerSpreeCredits, arena, int, def: 0
	* When the killed is on a spree, the killer receives this much credits extra for every kill */
	adata->killedSpreeCredits = cfg->GetInt(ch, "GameCredits", "KilledSpreeCredits", 0);
	
	/* cfghelp: GameCredits:KillCreditsShareBetweenFreqs, arena, int, def: 1, range:0-1
	* If you kill someone, do you get a portion of the credits for it on your other freqs */
	adata->killCreditsShareBetweenFreqs = cfg->GetInt(ch, "GameCredits", "KillCreditsShareBetweenFreqs", true);
	
	/* cfghelp: GameCredits:KillerTeammateCreditsMultiplier, arena, int, def: 0
	* How much a team mate gets for kills (in permille) */
	adata->killerTeammateCreditsMultiplier = cfg->GetInt(ch, "GameCredits", "KillerTeammateCreditsPromille", 0) / 1000.0;
	
	/* cfghelp: GameCredits:KillerTeammateCreditsMultiplier, arena, int, def: 0
	* The maximum distance a teammate receives credits for kills */
	adata->killerTeammateDistance = cfg->GetInt(ch, "GameCredits", "KillerTeammateDistance", 1440000);

	if (adata->killCreditsEnabled)
		adata->killCreditsEnabled = true;

	if (adata->killAnnounce)
		adata->killAnnounce = true;

	if (adata->killMaximumCredits < 0)
		adata->killMaximumCredits = 1000000;

	if (adata->killCreditsShareBetweenFreqs)
		adata->killCreditsShareBetweenFreqs = true;


	/* cfghelp: GameCredits:GoalCreditsEnabled, arena, int, def: 0, range:0-1
	* Enable credits for goals; Note that this requires a module calling credits->Goal(...) */
	adata->goalCreditsEnabled = cfg->GetInt(ch, "GameCredits", "GoalCreditsEnabled", false);
	
	/* cfghelp: GameCredits:GoalExtraCredits, arena, int, def: 500
	*  */
	adata->goalExtraCredits = cfg->GetInt(ch, "GameCredits", "GoalExtraCredits", 500);
	
	/* cfghelp: GameCredits:GoalCreditsPerPlaying, arena, int, def: 0
	*  */
	adata->goalCreditsPerPlaying = cfg->GetInt(ch, "GameCredits", "GoalCreditsPerPlaying", 0);
	
	/* cfghelp: GameCredits:GoalMaximumCredits, arena, int, def: 10000
	*  */
	adata->goalMaximumCredits = cfg->GetInt(ch, "GameCredits", "GoalMaximumCredits", 10000);
	
	/* cfghelp: GameCredits:GoalSplit, arena, int, def: 0, range:0-1
	*  */
	adata->goalSplit = cfg->GetInt(ch, "GameCredits", "GoalSplit", false);
	
	/* cfghelp: GameCredits:goalScorerBonusMultiplier, arena, int, def: 0
	*  */
	adata->goalScorerBonusMultiplier = cfg->GetInt(ch, "GameCredits", "GoalScorerBonusPromille", 0) / 1000.0;

	if (adata->goalCreditsEnabled)
		adata->goalCreditsEnabled = true;


	/* cfghelp: GameCredits:OreCreditsEnabled, arena, int, def: 0, range:0-1
	* Enable credits for ore mining; Note that this requires a module calling credits->Ore(...) */
	adata->oreCreditsEnabled = cfg->GetInt(ch, "GameCredits", "OreCreditsEnabled", false);
	
	/* cfghelp: GameCredits:OreCalculatePer, arena, int, def: 100
	* When calculating ore reward, divide it by this */
	adata->oreCalculatePer = cfg->GetInt(ch, "GameCredits", "OreCalculatePer", 100);
	
	/* cfghelp: GameCredits:OreExtraCredits, arena, int, def: 500
	*  */
	adata->oreExtraCredits = cfg->GetInt(ch, "GameCredits", "OreExtraCredits", 500);
	
	/* cfghelp: GameCredits:OreCreditsPerPlaying, arena, int, def: 0
	*  */
	adata->oreCreditsPerPlaying = cfg->GetInt(ch, "GameCredits", "OreCreditsPerPlaying", 0);
	
	/* cfghelp: GameCredits:OreMaximumCredits, arena, int, def: 10000
	*  */
	adata->oreMaximumCredits = cfg->GetInt(ch, "GameCredits", "OreMaximumCredits", 10000);
	
	/* cfghelp: GameCredits:OreSplit, arena, int, def: 0, range:0-1
	*  */
	adata->oreSplit = cfg->GetInt(ch, "GameCredits", "OreSplit", false);
	
	/* cfghelp: GameCredits:OreScorerBonusPromille, arena, int, def: 0
	*  How much more credits does the scorer get then his team members */
	adata->oreScorerBonusMultiplier = cfg->GetInt(ch, "GameCredits", "OreScorerBonusPromille", 0) / 1000.0;

	if (adata->oreCreditsEnabled)
		adata->oreCreditsEnabled = true;

	/* cfghelp: GameCredits:MaxCredits, arena, int, def: 10000
	*  The maximum amount of credits someone may have; The credits go to his team members when full */
	attrman->SetValue(
		&tgtArena, 
		settingSetter, 
		"GameCredits::maxcredits", 
		true, 
		ATTRMAN_NO_SHIP, 
		cfg->GetInt(ch, "GameCredits", "MaxCredits", 10000)
	);

	/* cfghelp: GameCredits:LVZEnabled, arena, int, def: 0, range:0-1
	* Enable lvz */
	adata->LVZEnabled = cfg->GetInt(ch, "GameCredits", "LVZEnabled", false);
	
	/* cfghelp: GameCredits:LVZEnabled, arena, int, def: 10
	* The maximum number of digits in the lvz */
	adata->LVZDigits = cfg->GetInt(ch, "GameCredits", "LVZDigits", 10);
	
	/* cfghelp: GameCredits:LVZObjectStart, arena, int, def: -1
	*  */
	adata->LVZObjectStart = cfg->GetInt(ch, "GameCredits", "LVZObjectStart", -1);
	
	/* cfghelp: GameCredits:LVZImageStart, arena, int, def: -1
	*  */
	adata->LVZImageStart = cfg->GetInt(ch, "GameCredits", "LVZImageStart", -1);

	if (adata->LVZEnabled)
	{
		adata->LVZEnabled = true;

		//sanity checks

		if (adata->LVZDigits < 1)
		{
			adata->LVZEnabled = false;
			lm->LogA(L_WARN, "gamecredits", arena, "Wrong value for gamecredits:LVZDigits");
		}
		else if (adata->LVZDigits > LVZ_MAXDIGITS)
		{
			adata->LVZDigits = LVZ_MAXDIGITS;
			lm->LogA(L_WARN, "gamecredits", arena, "Wrong value for gamecredits:LVZDigits");
		}
		if (adata->LVZObjectStart < 0)
		{
			adata->LVZEnabled = false;
			lm->LogA(L_WARN, "gamecredits", arena, "Wrong value for gamecredits:LVZObjectStart");
		}
		if (adata->LVZImageStart < 0)
		{
			adata->LVZEnabled = false;
			lm->LogA(L_WARN, "gamecredits", arena, "Wrong value for gamecredits:LVZImageStart");
		}
	}
	
	attrman->UnLock();

}

static bool StartGame (Arena *arena)
{
	assertlm(arena);
	Link *link;
	Player *p;
	int a;
	struct playerData *pdata;

	struct arenaData *adata = GetArenaData(arena);
	if (!adata->enabled) return false;


	adata->running = true;
	HashEnum(adata->playerCredits, hash_enum_afree, NULL);
	HashFree(adata->playerCredits);
	adata->playerCredits = HashAlloc();

	pd->Lock();
	FOR_EACH_PLAYER(p)
	{
		pdata = GetPlayerData(p);
		if (arena == pdata->arena)
		{
			for (a = 0; a < FREQCOUNT; a++)
			{
				pdata->cdata.credits[a] = 0;
			}
		}
	}
	pd->Unlock();

	return true;
}

static void StopGame(Arena *arena)
{
	assertlm(arena);

	struct arenaData *adata = GetArenaData(arena);
	struct playerData *pdata;
	Link *link;
	Player *p;
	int a;
	if (!adata->enabled) return;


	adata->running = false;
	HashEnum(adata->playerCredits, hash_enum_afree, NULL);
	HashFree(adata->playerCredits);
	adata->playerCredits = HashAlloc();

	pd->Lock();
	FOR_EACH_PLAYER(p)
	{
		pdata = GetPlayerData(p);
		if (arena == pdata->arena)
		{
			for (a = 0; a < FREQCOUNT; a++)
			{
				pdata->cdata.credits[a] = 0;
			}
		}
	}
	pd->Unlock();

}

static helptext_t credits_help =
	"Targets: none or private\n"
	"Args: [{-d}]\n"
	"Displays your credits. When private, displays the credits of the target\n"
	"{-d} can be used for extra details";

static void Ccredits(const char *tc, const char *params, Player *p, const Target *target)
{
	if (!p) return;
	struct arenaData *adata = GetArenaData(p->arena);
	if (!adata->enabled) return;

	int freq;
	Player *t;
	int a;
	if (!adata->running)
	{
		chat->SendCmdMessage(p, "You do not have any credits because the game is not running.");
		return;
	}

	if (target->type == T_PLAYER) //private command
	{
		t = target->u.p;
		if (!t) return;

		if (strstr(params, "-d")) //extra details
		{
			chat->SendCmdMessage(p, "Player %s has the following credits:", t->name);
			for (a = 0; a < FREQCOUNT; a++)
			{
				chat->SendCmdMessage(p, "Freq %i: %li credits.", a, GetCredits(t, a));
			}
		}
		else
		{
			freq = t->p_freq;

			if (freq >= 0 && freq < FREQCOUNT)
			{
				chat->SendCmdMessage(p, "Player %s has %li credits.", t->name, GetCredits(t, freq));
			}
			else
			{
				chat->SendCmdMessage(p, "Player %s can not have any credits.", t->name);
			}
		}
	}
	else //not a private command, assume public
	{
		if (strstr(params, "-d")) //extra details
		{
			chat->SendCmdMessage(p, "You have the following credits:");
			for (a = 0; a < FREQCOUNT; a++)
			{
				chat->SendCmdMessage(p, "Freq %i: %li credits.", a, GetCredits(p, a));
			}
		}
		else
		{
			freq = p->p_freq;

			if (freq >= 0 && freq < FREQCOUNT)
			{
				chat->SendCmdMessage(p, "You have %li credits.", GetCredits(p, freq));
			}
			else
			{
				chat->SendCmdMessage(p, "Your frequency can not have any credits.");
			}
		}
	}

}
static helptext_t grant_help =
	"Targets: private\n"
	"Args: [{-q}] [{-o}] [{-fX}] <amount> [<message>]\n"
	"Adds the specified amount of credits to the targeted player\n"
	"{-q}  - Player will not be notified. However your <message> will be logged.\n"
	"{-fX} - Add the specified amount to a specific freq of the player, where X is the freq number.\n"
	"{-o}  - Add a percentage of the specified amount to the other freqs of the player. (Credits:OtherFreqGainPermille)\n"
	"For a list of the amount of credits the player has for every freq, use ?credits -d";

static void Cgrant(const char *tc, const char *paramaters, Player *p, const Target *target)
{
	if (!p) return;
	struct arenaData *adata = GetArenaData(p->arena);
	if (!adata->enabled) return;

	const char *params = paramaters;

	char *next;
	Player *t;

	bool quiet = false; // {-q}
	int freq = -1; // {-fX} (-1 = -f not given)
	bool otherFreqGain = false; // {-o};
	int amount = 0; //<amount>
	char *message; // [<message>]

	if (!adata->running)
	{
		chat->SendCmdMessage(p, "Can not grant a player if the game is not running");
		return;
	}

	//get the flags
	while (params != NULL)
	{
		if (strncmp(params, "-q", 2) == 0) //quiet
		{
			quiet = true;
		}
		else if (strncmp(params, "-o", 2) == 0) //other freq gain
		{
			otherFreqGain = true;
		}
		else if (strncmp(params, "-f", 2) == 0) //alternate freq
		{
			params += 2; //after the -f
			freq = strtol(params, &next, 0); //if there is no number found, strtol returns NULL. This will give an error
			if (freq < 0 || freq >= FREQCOUNT)
			{
				chat->SendCmdMessage(p, "Invalid freq; Please use a freq between 0 and %d", FREQCOUNT-1);
				return;
			}
			params = next; //no need to re itterate this number again (in strchr())

		}
		else
		{
			break;
		}

		params = strchr(params, ' ');
		if (params) //check so that params can still == NULL
		{
			params++; //we want *after* the space
		}
	}

	//get the arguments
	if (params == NULL)
	{
		chat->SendCmdMessage(p, "Invalid usage");
		return;
	}

	amount = strtol(params, &next, 0);

	if (params == next)
	{
		chat->SendCmdMessage(p, "Invalid amount");
		return;
	}

	while (*next == ' ') next++; //remove whitespace before the message

	message = next;
	if (message[0] == 0)
	{
		message = NULL;
	}

	//parsing complete

	if (target->type == T_PLAYER) //private command
	{
		t = target->u.p;
		if (freq < 0) freq = t->p_freq;

		if (t->p_freq < 0 || t->p_freq >= FREQCOUNT)
		{
			chat->SendCmdMessage(p, "Invalid freq; Please grant a player on a freq between 0 and %d", FREQCOUNT-1);
			return;
		}

		AddCreditsPlayer(t, freq, amount, otherFreqGain, false);
		if (quiet)
		{
			chat->SendMessage(p, "Quietly granted player %s %i credits.", t->name, amount);
			if (message == NULL)
			{
				lm->LogP(L_INFO, "gamecredits", p, "Quietly granted player %s %i credits.", t->name, amount);
			}
			else
			{
				lm->LogP(L_INFO, "gamecredits", p, "Quietly granted player %s %i credits (%s).", t->name, amount, message);
			}
		}
		else
		{
			if (message == NULL)
			{
				chat->SendMessage(t, "You were granted %i credits.", amount);
				chat->SendMessage(p, "Granted player %s %i credits.", t->name, amount);
				lm->LogP(L_INFO, "gamecredits", p, "Granted player %s %i credits.", t->name, amount);
			}
			else
			{
				chat->SendMessage(t, "You were granted %i credits (%s).", amount, message);
				chat->SendMessage(p, "Granted player %s %i credits (%s).", t->name, amount, message);
				lm->LogP(L_INFO, "gamecredits", p, "Granted player %s %i credits (%s).", t->name, amount, message);
			}

		}
	}
	else
	{
		chat->SendCmdMessage(p, "Invalid target; Use private messages.");
		return;
	}
	lm->LogP(L_DRIVEL, "gamecredits", p, "<< Cgrant(%s, %s, player, target)", tc, params);
}

static void Kill(Arena *arena, Player *killer, Player *killed, int bounty, int flags, int pts, int green)
{
	struct arenaData *adata = GetArenaData(arena);
	if (!adata->enabled) return;
	if (!adata->killCreditsEnabled) return;

	struct playerData *killerData, *killedData;

	killerData = GetPlayerData(killer);
	killedData = GetPlayerData(killed);

	int killerSpree = 0;
	if (IS_STANDARD(killer) && IS_STANDARD(killed))
	{
		killerSpree = killerData->killsWithoutDeath++; // no bonus on first kill
	}

	int killedSpree = 0;
	if (IS_STANDARD(killed))
	{
		killedSpree = killedData->killsWithoutDeath;
		killedData->killsWithoutDeath = 0;
	}

	if (!IS_STANDARD(killer) || !IS_STANDARD(killed)) return;

	if (killer->p_freq == killed->p_freq)
	{
		if (adata->killAnnounce)
			chat->SendMessage(killer, "No reward for teamkill of %s.", killed->name);
		return;
	}

	attrman->Lock();
	double credits = killerSpree * adata->killerSpreeCredits;
	credits += killedSpree * adata->killedSpreeCredits;
	credits += adata->killExtraCredits;
	credits += credits * (attrman->GetPlayerValue(killer, "gamecredits::killbonuspermille", NULL) / 1000.0);
	attrman->UnLock();

	if (credits > adata->killMaximumCredits)
	{
		credits = adata->killMaximumCredits;
	}
	else if (credits < 0.0)
	{
		credits = 0.0;
	}

	if (adata->killAnnounce)
		chat->SendMessage(killer, "You received %d credits for killing %s.", (int) credits, killed->name);

	AddCreditsPlayer(killer, killer->p_freq, (int) credits, adata->killCreditsShareBetweenFreqs, true);

	double maxReward = credits * adata->killerTeammateCreditsMultiplier;

	Player *p;
	Link *link;

	pd->Lock();
	FOR_EACH_PLAYER(p)
	{
		if (p->arena == killer->arena && p->p_freq == killer->p_freq &&
				p->p_ship != SHIP_SPEC && p != killer &&
				!(p->position.status & STATUS_SAFEZONE))
		{
			int xdelta = p->position.x - killer->position.x;
			int ydelta = p->position.y - killer->position.y;
			double distMultiplier = ((double)(xdelta * xdelta + ydelta * ydelta)) / adata->killerTeammateDistance;

			double creditsTeamMate = maxReward * exp(-distMultiplier);

			AddCreditsPlayer(p, p->p_freq, (int) creditsTeamMate, false, false);
		}
	}
	pd->Unlock();


}

//This is not a callback, it is an interface used by points_cncgoal
static bool Goal(Arena *arena, Player *p, int freq, int bid, int x, int y)
{
	struct arenaData *adata = GetArenaData(arena);
	if (!adata->enabled) return 0;
	if (!adata->goalCreditsEnabled)
		return false; //return false = do not suppress default arena messages


	Player *i;
	Link *link;
	int players = 0;
	int playersOnTeam = 0;
	LinkedList teamset = LL_INITIALIZER, nmeset = LL_INITIALIZER;

	pd->Lock();
	FOR_EACH_PLAYER(i)
	{
		if (i->status == S_PLAYING &&
				i->arena == arena &&
				i->p_ship != SHIP_SPEC &&
				i->type != T_FAKE)
		{
			players++;
			if (i->p_freq == freq)
			{
				LLAdd(&teamset, i);
				playersOnTeam++;
			}
			else
			{
				LLAdd(&nmeset, i);
			}
		}
	}
	pd->Unlock();

	if (adata->running)
	{
		attrman->Lock();
		long credits = adata->goalExtraCredits + (players * adata->goalCreditsPerPlaying);
		credits += credits * (attrman->GetPlayerValue(p, "gamecredits::goalbonuspermille", NULL) / 1000.0);

		if (playersOnTeam == 1) playersOnTeam = 2;
		if (adata->goalSplit && playersOnTeam > 0)
			credits /= playersOnTeam;

		if (credits < 0) credits = 0;
		if (credits > adata->goalMaximumCredits) credits = adata->goalMaximumCredits;

		AddCreditsFreq(arena, freq, credits, true, true);
		AddCreditsPlayer(p, freq, credits * adata->goalScorerBonusMultiplier, true, true);

		chat->SendSetSoundMessage(&teamset, SOUND_GOAL, "%s harvested tiberium! Reward: %li credits.", p->name, credits);
		chat->SendSetSoundMessage(&nmeset, SOUND_GOAL, "%s harvested tiberium for the enemy! Reward: %li credits.", p->name, credits);
		attrman->UnLock();
	}
	else
	{
		chat->SendSetSoundMessage(&teamset, SOUND_GOAL, "%s harvested tiberium!", p->name);
		chat->SendSetSoundMessage(&nmeset, SOUND_GOAL, "%s harvested tiberium for the enemy!", p->name);
	}
	LLEmpty(&teamset);
	LLEmpty(&nmeset);


	return true;
}

static void MineOre(Arena *arena, Player *miner, int freq, int ore)
{
	struct arenaData *adata = GetArenaData(arena);
	if (!adata->enabled || !adata->oreCreditsEnabled) return;


	Player *i;
	Link *link;
	int players = 0;
	int playersOnTeam = 0;
	LinkedList teamset = LL_INITIALIZER, nmeset = LL_INITIALIZER;

	pd->Lock();
	FOR_EACH_PLAYER(i)
	{
		if (i->status == S_PLAYING &&
				i->arena == arena &&
				i->p_ship != SHIP_SPEC &&
				i->type != T_FAKE)
		{
			players++;
			if (i->p_freq == freq)
			{
				LLAdd(&teamset, i);
				playersOnTeam++;
			}
			else
			{
				LLAdd(&nmeset, i);
			}
		}
	}
	pd->Unlock();

	if (adata->running)
	{
		attrman->Lock();
		long credits = adata->oreExtraCredits + (players * adata->oreCreditsPerPlaying);
		credits += credits * (attrman->GetPlayerValue(miner, "gamecredits::orebonuspermille", NULL) / 1000.0);
		credits = (credits * ore) / adata->oreCalculatePer;
		attrman->UnLock();


		if (playersOnTeam == 1) playersOnTeam = 2;
		if (adata->oreSplit && playersOnTeam > 0)
			credits /= playersOnTeam;

		if (credits < 0) credits = 0;
		if (credits > adata->oreMaximumCredits) credits = adata->oreMaximumCredits;

		AddCreditsFreq(arena, freq, credits, true, true);


		AddCreditsPlayer(miner, freq, credits * adata->oreScorerBonusMultiplier, true, true);

		/*chat->SendSetSoundMessage(&teamset, SOUND_GOAL, "%s harvested tiberium! Reward: %li credits.", p->name, credits);
		chat->SendSetSoundMessage(&nmeset, SOUND_GOAL, "%s harvested tiberium for the enemy! Reward: %li credits.", p->name, credits);*/
		
	}
	else
	{
		/*chat->SendSetSoundMessage(&teamset, SOUND_GOAL, "%s harvested tiberium!", p->name);
		chat->SendSetSoundMessage(&nmeset, SOUND_GOAL, "%s harvested tiberium for the enemy!", p->name);*/
	}
	LLEmpty(&teamset);
	LLEmpty(&nmeset);
}

static void PlayerAction(Player *p, int action, Arena *arena)
{
	pd->Lock();
	if (action == PA_ENTERARENA)
	{
		struct arenaData *adata = GetArenaData(arena);
		int a;
		if (adata->running && IS_STANDARD(p))
		{
			struct creditData *cdata = GetCreditData(arena, p);
			struct playerData *pdata = GetPlayerData(p);

			pdata->killsWithoutDeath = 0;
			for (a = 0; a < FREQCOUNT; a++)
				pdata->cdata.credits[a] = cdata->credits[a];
		}
	}
	else if (action == PA_LEAVEARENA)
	{
		struct arenaData *adata = GetArenaData(arena);
		int a;
		if (adata->running && IS_STANDARD(p))
		{

			struct creditData *cdata = GetCreditData(arena, p);
			struct playerData *pdata = GetPlayerData(p);

			for (a = 0; a < FREQCOUNT; a++)
				cdata->credits[a] = pdata->cdata.credits[a];

			//*cdata = pdata->cdata;

		}
	}
	pd->Unlock();
}

static void ShipFreqChangeCB(Player *p, int newship, int oldship, int newfreq, int oldfreq)
{
	struct playerData *pdata = GetPlayerData(p);
	pdata->killsWithoutDeath = 0;
}

EXPORT const char info_gamecredits[] = "GameCredits by JoWie ("BUILDDATE")\n"
				   "This is an attach module.\n";

static void ReleaseInterfaces()
{
	mm->ReleaseInterface(chat   );
	mm->ReleaseInterface(ml     );
	mm->ReleaseInterface(cfg    );
	mm->ReleaseInterface(cmd    );
	mm->ReleaseInterface(aman   );
	mm->ReleaseInterface(pd     );
	mm->ReleaseInterface(prng   );
	mm->ReleaseInterface(lm     );
	mm->ReleaseInterface(objs   );
	mm->ReleaseInterface(attrman);
}

EXPORT int MM_gamecredits(int action, Imodman *mm_, Arena *arena)
{
	Link *link;
	Player *p;
	int a;
	struct playerData *pdata;

	if (action == MM_LOAD)
	{
		mm = mm_;

		chat    = mm->GetInterface(I_CHAT	    , ALLARENAS);
		ml      = mm->GetInterface(I_MAINLOOP	, ALLARENAS);
		cfg     = mm->GetInterface(I_CONFIG	  , ALLARENAS);
		cmd     = mm->GetInterface(I_CMDMAN	  , ALLARENAS);
		aman    = mm->GetInterface(I_ARENAMAN	, ALLARENAS);
		pd      = mm->GetInterface(I_PLAYERDATA      , ALLARENAS);
		prng    = mm->GetInterface(I_PRNG	    , ALLARENAS);
		lm      = mm->GetInterface(I_LOGMAN	  , ALLARENAS);
		attrman = mm->GetInterface(I_ATTRMAN	 , ALLARENAS);
		objs    = mm->GetInterface(I_OBJECTS	 , ALLARENAS); //optional

		if (!chat || !ml || !cfg || !cmd || !cfg || !aman || !pd  || !prng || !lm || !attrman)
		{
			ReleaseInterfaces();

			return MM_FAIL;
		}

		playerKey = pd->AllocatePlayerData(sizeof(struct playerData));
		arenaKey = aman->AllocateArenaData(sizeof(struct arenaData));

		if (arenaKey == -1 || playerKey == -1) // check if we ran out of memory
		{
			if (arenaKey  != -1) // free data if it was allocated
				aman->FreeArenaData(arenaKey);

			if (playerKey != -1) // free data if it was allocated
				pd->FreePlayerData (playerKey);

			ReleaseInterfaces();

			return MM_FAIL;
		}

		if (objs) ml->SetTimer(UpdateLVZ_timer, LVZ_UPDATETIMER, LVZ_UPDATETIMER, ALLARENAS, ALLARENAS);

		attrman->Lock();
		settingSetter = attrman->RegisterSetter();
		attrman->UnLock();

		return MM_OK;

	}
	else if (action == MM_UNLOAD)
	{
		if (credits.head.arena_registrations)
			return MM_FAIL;

		attrman->Lock();
		attrman->UnregisterSetter(settingSetter);
		attrman->UnLock();

		ml->ClearTimer(UpdateLVZ_timer, ALLARENAS);

		mm->UnregCallback(CB_PLAYERACTION, PlayerAction, ALLARENAS);
		mm->UnregCallback(CB_KILL, Kill, ALLARENAS);

		cmd->RemoveCommand("credits", Ccredits, ALLARENAS);
		cmd->RemoveCommand("creds", Ccredits, ALLARENAS);
		cmd->RemoveCommand("money", Ccredits, ALLARENAS);
		cmd->RemoveCommand("gold", Ccredits, ALLARENAS);
		cmd->RemoveCommand("grant", Cgrant, ALLARENAS);

		aman->FreeArenaData(arenaKey);
		pd->FreePlayerData(playerKey);

		ReleaseInterfaces();

		return MM_OK;
	}
	else if (action == MM_ATTACH)
	{
		mm->RegInterface(&credits, arena);

		mm->RegCallback(CB_PLAYERACTION, PlayerAction, arena);
		mm->RegCallback(CB_KILL, Kill, arena);
		mm->RegCallback(CB_SHIPFREQCHANGE, ShipFreqChangeCB, arena);

		cmd->AddCommand("credits", Ccredits, arena, credits_help);
		cmd->AddCommand("creds", Ccredits, arena, credits_help);
		cmd->AddCommand("money", Ccredits, arena, credits_help);
		cmd->AddCommand("gold", Ccredits, arena, credits_help);
		cmd->AddCommand("grant", Cgrant, arena, grant_help);

		ReadSettings(arena);


		return MM_OK;
	}
	else if (action == MM_DETACH)
	{
		if (mm->UnregInterface(&credits, arena))
			return MM_FAIL;

		struct arenaData *adata = GetArenaData(arena);

		mm->UnregCallback(CB_PLAYERACTION, PlayerAction, arena);
		mm->UnregCallback(CB_KILL, Kill, arena);
		mm->UnregCallback(CB_SHIPFREQCHANGE, ShipFreqChangeCB, arena);

		cmd->RemoveCommand("credits", Ccredits, arena);
		cmd->RemoveCommand("creds", Ccredits, arena);
		cmd->RemoveCommand("money", Ccredits, arena);
		cmd->RemoveCommand("gold", Ccredits, arena);
		cmd->RemoveCommand("grant", Cgrant, arena);

		adata->running = false;
		HashEnum(adata->playerCredits, hash_enum_afree, NULL);
		HashFree(adata->playerCredits);
		adata->playerCredits = HashAlloc();

		pd->Lock();
		FOR_EACH_PLAYER(p)
		{
			pdata = GetPlayerData(p);
			if (arena == pdata->arena)
			{
				for (a = 0; a < FREQCOUNT; a++)
				{
					pdata->cdata.credits[a] = 0;
				}
			}
		}
		pd->Unlock();


		return MM_OK;
	}

	return MM_FAIL;
}

