/* $Id: advbuy.h 35 2008-03-25 00:21:24Z joris $ */
#ifndef __ADVBUY_H
#define __ADVBUY_H
#include <stdbool.h>

#define I_ADVBUY "advbuy-1"
/* pyinclude: advbuy/advbuy.h */

typedef struct Iadvbuy
{
	INTERFACE_HEAD_DECL
	/* pyint: use */

	bool (*StartGame)(Arena *arena);
	/* pyint: arena_not_none -> int */
	
	void (*StopGame)(Arena *arena);
	/* pyint: arena_not_none -> void */

} Iadvbuy;

#endif
