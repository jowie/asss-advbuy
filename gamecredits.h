/* $Id: credits.h 213 2009-08-15 16:47:02Z joris $ */
#ifndef __CREDITS_H
#define __CREDITS_H
#include <stdbool.h>

#define I_GAMECREDITS "gamecredits-4"
/* pyinclude: advbuy/gamecredits.h */

typedef struct Igamecredits
{
	INTERFACE_HEAD_DECL
	/* pyint: use */

	/** Add credits for a specific player
		freq - The freq for the specified player. -1 means current freq
		credits - The amount of credits
		otherFreqGain - add a percentage of the credits to other freqs of the player aswell
	*/
	void (*AddCreditsPlayer)(Player *p, int freq, long credits, bool otherFreqGain, bool overflowCreditsIntoTeamMembers);
	/* pyint: player_not_none, int, int, int, int -> void */
	
	void (*AddCreditsFreq)(Arena *arena, int freq, long credits, bool otherFreqGain, bool overflowCreditsIntoTeamMembers);
	/* pyint: arena_not_none, int, int, int, int -> void */
	
	long (*SetCredits)(Player *p, int freq, long credits);
	/* pyint: player_not_none, int, int -> int */
	
	long (*GetCredits)(Player *p, int freq);
	/* pyint: player_not_none, int -> int */
	
	void (*ResetCredits)(Player *p);
	/* pyint: player_not_none -> void */
	
	bool (*StartGame)(Arena *arena);
	/* pyint: arena_not_none -> int */
	
	void (*StopGame)(Arena *arena);
	/* pyint: arena_not_none -> void */
	
	bool  (*Goal)(Arena *arena, Player *p, int freq, int bid, int x, int y); // true = suppress default soccer arena message (this module already sent one)
	/* pyint: arena_not_none, player_not_none, int, int, int, int -> int */
	
	void (*MineOre)(Arena *arena, Player *miner, int freq, int ore);
	/* pyint: arena_not_none, player_not_none, int, int -> void */
	

} Igamecredits;

#endif
